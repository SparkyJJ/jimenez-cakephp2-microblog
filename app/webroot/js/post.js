$(document).ready(function () {

    $('body').on('click', '.like-post', function () {
        $(this).children('form').submit()
    })

    $('.like-anchor, .sidemenu.delete.item').removeAttr('onclick')
    $('.like-anchor, .sidemenu.delete.item').removeAttr('href')

    $('body').on('submit', '.like-post form', function (e) {
        e.preventDefault()
        let like_form = $(this)
        let url = $(this).attr('action')
        $.ajax({
            url: url,
            type: 'POST',
            data: $(this).serialize(),
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-CSRF-Token', like_form.find('input[name="data[_Token][key]').val());
            },
            success: function (result) {
                let data = JSON.parse(result)
                let element = $(data.content)
                like_form.parent().toggleClass('basic')
                like_form.parent().siblings('.like-count').html(data.count)
                like_form.parent().html(element)
                element.removeAttr('onclick')
                element.removeAttr('href')
            }
        })
    })

    $('body').on('click', '.share-post', function () {
        let id = $(this).closest('.post-segment').data('post-id')
        let userid = $(this).closest('.post-segment').data('user-id')
        $.ajax({
            url: '/posts/view_share/' + id + '/' + userid,
            success: function (result) {
                let element = $(result)
                $('.ui.general.modal').html(element)
                $('.ui.general.modal').addClass('small')
                $('.ui.general.modal').removeClass('tiny')
                $('.ui.general.modal').modal('show')
                element.find('.post-segment').transition('fade up', {
                    duration: 800
                })
            }
        })
    })

    $('body').on('click', '.comment-post', function () {
        $(this).toggleClass('basic')
        $(this).closest('.extra').children('.comments-segment').transition('drop', {
            onComplete: function () {
                $(this).closest('.extra').find('.comment-field-form textarea').focus()
            }
        })
    })

    $('body').on('click', '.sidemenu.delete.item', function (e) {
        e.preventDefault()
        // $(this).siblings('form').submit()
        $('.ui.general.modal').html(
            '<h2 class="ui icon header"><i class="yellow exclamation triangle icon"></i>' +
            '<div class="content">Are you sure you want to delete this post?</div></h2>' +
            '<div class="actions"><div class="ui black deny button">Nope</div>' +
            '<div class="ui positive right labeled icon button" onclick="document.'+
            $(this).siblings('form').attr('id')+
            '.submit()">Yep, Delete it<i class="trash icon"></i></div></div >'
        )
        $('.ui.general.modal').addClass('tiny')
        $('.ui.general.modal').removeClass('small')
        $('.ui.general.modal').modal('show')
    })
})
