$(document).ready(function () {

    $('body').on('click', '.edit.comment', function () {
        let comment = $(this)
        let thread = comment.closest('.ui.threaded.comments')
        let post = comment.closest('.post-segment')
        let id = comment.closest('.post-comment').data('comment-id')
        let reply = comment.parent().siblings('.edit-field')
        comment.toggleClass('active')
        if (reply.length > 0) {
            if (reply.hasClass('visible')) {
                reply.transition('drop', {
                    onComplete: function () {
                        comment.parent().siblings('.comment-text').transition('fade')
                    }
                })
            } else {
                comment.parent().siblings('.comment-text').transition('fade', {
                    onComplete: function () {
                        reply.transition('drop')
                    }
                })
            }
        } else {
            $.ajax({
                url: "/comments/view_edit_field/" + id + "/" + post.data('post-id'),
                success: function (result) {
                    element = $(result)
                    comment.parent().siblings('.comment-text').after(element)
                    comment.parent().siblings('.comment-text').transition('drop')
                    element.hide().transition('fade')
                }
            })
        }
    })

    $('body').on('click', '.delete.comment', function () {
        // $(this).children('a').click()
        $('.ui.general.modal').html(
            '<h2 class="ui icon header"><i class="yellow exclamation triangle icon"></i>' +
            '<div class="content">Are you sure you want to delete this comment?</div></h2>' +
            '<div class="actions"><div class="ui black deny button">Nope</div>' +
            '<div class="ui positive right labeled icon button" onclick="deleteComment(' +
            $(this).children('form').attr('id') +
            ')">Yep, Delete it<i class="trash icon"></i></div></div >'
        )
        $('.ui.general.modal').addClass('tiny')
        $('.ui.general.modal').removeClass('small')
        $('.ui.general.modal').modal('show')
    })

    $('body').on('click', '.delete.comment a', function () {
        e.preventDefault()
    })

    $('body').on('click', '.reply.comment', function () {
        let comment = $(this)
        let thread = comment.closest('.ui.threaded.comments')
        let post = comment.closest('.post-segment')
        let id = comment.closest('.post-comment').data('comment-id')
        let reply = comment.parent().parent().siblings('.reply-field')
        comment.toggleClass('active')
        if (reply.length > 0) {
            reply.transition('drop')
        } else {
            $.ajax({
                url: "/comments/view_reply_field/" + id + "/" + post.data('post-id'),
                success: function (result) {
                    element = $(result)
                    comment.closest('.post-comment').append(element)
                    element.hide().transition('drop')
                }
            })
        }
    })

    $('body').on('submit', '.comment-field-form', function (e) {
        e.preventDefault()

        let form = $(this)
        let url = form.attr('action')
        let thread = form.siblings('.ui.grid').find('.ui.threaded.comments')
        let post_id = form.closest('.post-segment').data('post-id')
        form.children('.ui.segment').children('.ui.dimmer').addClass('active')
        $.ajax({
            type: 'POST',
            url: url,
            data: form.serialize(),
            success: function (result) {
                let data = JSON.parse(result)
                let element = $(data.content)
                form.closest('.comments-segment').replaceWith(element)
                element.hide().transition('fade down')
                form.children('.ui.segment').children('.ui.dimmer').removeClass('active')
                element.find('textarea').val('')
                element.closest('.extra').find('.comment-count').html(data.count)
            },
            error: function (result) {
                form.children('.ui.segment').children('.ui.dimmer').removeClass('active')
            }
        })
    })

    $('body').on('submit', '.reply-comment-field-form', function (e) {
        e.preventDefault()

        let form = $(this)
        let comment = form.closest('.post-comment')
        let url = form.attr('action')
        let thread = form.siblings('.ui.threaded.comments')
        form.siblings('.ui.dimmer').addClass('active')
        $.ajax({
            type: 'POST',
            url: url,
            data: form.serialize(),
            success: function (result) {
                let data = JSON.parse(result)
                let element = $(data.content)
                if (comment.find('.comments').length <= 0) {
                    comment.find('.content').after("<div class='comments reply-comments'></div>")
                }
                comment.find('.comments').append(element)
                element.hide().transition('drop')
                form[0].reset()
                form.closest('.extra').find('.comment-count').html(data.count)
                form.siblings('.ui.dimmer').removeClass('active')
            },
            error: function (result) {
                form.siblings('.ui.dimmer').removeClass('active')
            }
        })
    })

    $('body').on('submit', '.edit-comment-field-form', function (e) {
        e.preventDefault()

        let form = $(this)
        let comment = form.closest('.post-comment')
        let url = form.attr('action')
        let thread = form.siblings('.ui.threaded.comments')
        form.siblings('.ui.dimmer').addClass('active')
        $.ajax({
            type: 'POST',
            url: url,
            data: form.serialize(),
            success: function (result) {

                let data = JSON.parse(result)
                let element = $(data.content)
                comment.transition('scale', {
                    onComplete: function () {
                        let reply = comment.children('.reply-comments')
                        comment.replaceWith(element)
                        element.append(reply)
                        element.hide().transition('scale')
                    }
                })
                form.siblings('.ui.dimmer').removeClass('active')
            },
            error: function (result) {
                form.siblings('.ui.dimmer').removeClass('active')
            }
        })
    })

    $('body').on('click', '.next-comments, .prev-comments', function (e) {
        e.preventDefault()
        let anchor = $(this)
        let url = anchor.children('a').attr('href')
        $.ajax({
            url: url,
            success: function (result) {
                element = $(result)
                if (anchor.hasClass('next-comments')) {
                    anchor.closest('.comments-segment').children('.ui.grid').transition('fade left', {
                        onComplete: function () {
                            anchor.closest('.comments-segment').children('.ui.grid').replaceWith(element)
                            element.hide().transition('fade right')
                        }
                    })
                } else {
                    anchor.closest('.comments-segment').children('.ui.grid').transition('fade right', {
                        onComplete: function () {
                            anchor.closest('.comments-segment').children('.ui.grid').replaceWith(element)
                            element.hide().transition('fade left')
                        }
                    })

                }
            }
        })
    })
})

function deleteComment (form) {
    let delete_form = $(form)
    let url = delete_form.attr('action')

    let comment = delete_form.parent()
    let replyThread = comment.closest('.reply-comments')
    let thread = comment.closest('.ui.threaded.comments')
    let post = comment.closest('.post-segment')
    $.ajax({
        url: url,
        type: 'POST',
        data: delete_form.serialize(),
        beforeSend: function (xhr) {
            xhr.setRequestHeader('X-CSRF-Token', delete_form.find('input[name="data[_Token][key]').val());
        },
        success: function (result) {
            let data = JSON.parse(result)
            comment.closest('.post-comment').transition('fly left', {
                onComplete: function () {
                    $(this).remove()
                    if (thread[0].childElementCount <= 0) {
                        thread.toggleClass('rm-margin comment-thread')
                    }
                    post.find('.comment-count').html(data.count)
                    if (replyThread.length > 0) {
                        if (replyThread[0].childElementCount <= 0) {
                            replyThread.remove()
                        }
                    }
                    let element = $(data.content)
                    post.find('.comments-segment').replaceWith(element)
                    element.hide().transition('fade down')
                }
            })
        }
    })
}