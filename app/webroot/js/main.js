$(document).ready(function () {
    $('.ui.segment.post-loading').remove()

    $('.ui.segment.post-segment').transition('fade down', {
        duration: 1000,
        onComplete: function () {
            $('.ui.sticky').sticky('refresh')
        }
    })

    $('.search.link.icon').click(function () {
        $(this).closest('form').submit()
    })

    $('body').on('submit', '#PostAddForm.post-field-form', function (e) {
        e.preventDefault()
        let form = $(this)
        let loader = $(this).find('.ui.dimmer')
        let data = new FormData(form[0]);
        loader.addClass('active')
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            processData: false,  // Important!
            contentType: false,
            cache: false,
            url: form.attr('action'),
            data: data,
            success: function (result) {
                let data = JSON.parse(result)

                if (typeof data.content !== 'undefined') {
                    if ($('.feed-segment').length) {
                        if ($('.ui.general.modal').hasClass('active')) {
                            $('.ui.general.modal').modal('hide')
                        }
                        form[0].reset()
                        form.find('.bodycount').progress('set progress', 0)
                        form.find('.field.preview').empty()
                        resetFeed(data.content, loader)
                    } else {

                        location.href = '/main'
                        let top = $('.feed-segment')['0'].offsetTop
                        $("html, body").animate({ scrollTop: top }, "slow");
                        return;
                    }
                }

                if (typeof data.message !== 'undefined') {
                    if (data.messageType === 'modal') {
                        $('.ui.general.modal').html(data.message)
                        $('.ui.general.modal').addClass('tiny')
                        $('.ui.general.modal').removeClass('small')
                        $('.ui.general.modal').modal('show')
                    } else {
                        form.prepend(data.message)
                    }
                }

                loader.removeClass('active')
            },
            error: function (xhr) {
                loader.removeClass('active')
                form[0].reset()
                form.find('.bodycount').progress('set progress', 0)
                form.find('.field.preview').empty()
            }
        })
    })

    $('.main-segment').on('click', '.ui.main.pagination.menu .item', function () {
        let anchor = $(this).children('a')
        let url = anchor.attr('href')
        if (typeof url !== 'undefined') {
            let loader = $(this).closest('.feed-segment').find('.main-dimmer')
            loader.addClass('active')
            $.ajax({
                url: url,
                success: function (result) {
                    let data = JSON.parse(result)
                    resetFeed(data.content, loader)
                },
                error: function (result) {
                    loader.removeClass('active')
                }
            })
        }
    })

    $('.main-segment').on('click', '.ui.main.pagination.menu .item a', function (e) {
        e.preventDefault()
    })

    $('.main-segment').on('click', '.ui.search.pagination.menu .item', function (e) {
        location.href = $(this).children('a').attr('href')
    })

    $('.ui.general.modal').modal()

    $('body').on('click', '.user-card.card div[class="content"], .user-card.card div.image', function () {
        location.href = '/profile/view/' + $(this).parent().data('card-id')
    })

    $('.ui.dropdown.post-setting').dropdown()

    $('body').on('click', '.ui.search.bottom.button', function () {
        $(this).closest('.search-sub-form').submit()
    })

    function resetFeed(ajax, loader) {
        element = $(ajax)
        $('.feed-segment').replaceWith(element)
        element.children('.post-segment').transition('fade down', { duration: 1000 })
        $('.ui.dropdown.post-setting').dropdown()
        let top = $('.feed-segment')['0'].offsetTop
        $("html, body").animate({ scrollTop: top }, "slow");
        element.find('.post-segment .basic.segment .post-segment').transition('fade down')
        $('.like-anchor, .sidemenu.delete.item').removeAttr('onclick')
        $('.like-anchor, .sidemenu.delete.item').removeAttr('href')
    }
})
