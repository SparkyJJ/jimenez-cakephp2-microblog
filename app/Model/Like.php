<?php
App::uses('AppModel', 'Model');
class Like extends AppModel
{
    public $belongsTo = array(
        'Post' => [
            'className' => 'Post',
            'foreignKey' => 'post_id',
            'fields' => ['id', 'user_id', 'post_id'],
            'counterCache' => true
        ]
    );

    var $validate = [
        "user_id" => [
            "unique" => [
                "rule" => ["checkUnique", ["user_id", "post_id"]],
            ]
        ]
    ];

    function checkUnique($data, $fields)
    {
        if (!is_array($fields)) {
            $fields = array($fields);
        }
        foreach ($fields as $key) {
            $tmp[$key] = $this->data[$this->name][$key];
        }
        if (isset($this->data[$this->name][$this->primaryKey])) {
            $tmp[$this->primaryKey] = "<>" . $this->data[$this->name][$this->primaryKey];
        }
        return $this->isUnique($tmp, false);
    }
}
