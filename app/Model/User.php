<?php

App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel
{
    public $actsAs = [
        'Upload.Upload' => [
            'profile_picture' => [
                'thumbnailMethod' => 'php',
                'fields' => [
                    'dir' => 'profile_picture_dir'
                ],
                'thumbnailSizes' => [
                    'lg' => '200x200',
                    'md' => '100x100',
                    'sm' => '50x50'
                ],
                'path' => '{ROOT}webroot{DS}img{DS}{model}{DS}{field}{DS}',
                'deleteOnUpdate' => true
            ]
        ],
        'Containable',
        'Search.Searchable'
    ];

    public $filterArgs = array(
        'username' => array(
            'type' => 'like',
            'field' => 'username'
        ),
        'email' => array(
            'type' => 'like',
            'field' => 'email'
        ),
        'first_name' => array(
            'type' => 'like',
            'field' => 'first_name'
        ),
        'last_name' => array(
            'type' => 'like',
            'field' => 'last_name'
        )
    );


    public $hasMany = array(
        'Follower' => array(
            'className' => 'Follower',
            'foreignKey' => 'user_follower_id',
        ),
        'Followee' => array(
            'className' => 'Follower',
            'foreignKey' => 'user_followee_id',
        )
    );

    public $validate = [
        'username' => [
            'notBlank' => [
                'rule' => 'notBlank',
                'message' => 'Please fill up username field.'
            ],
            'alphaNumericUnderscore' => [
                'rule' => '/^[a-zA-Z0-9_]*$/',
                'required' => true,
                'last' => false,
                'message' => 'Letter, numbers, and underscores only.'
            ],
            'between' => [
                'rule' => [
                    'lengthBetween',
                    5,
                    15
                ],
                'last' => false,
                'message' => 'Username should be between 5 to 15 characters.'
            ],
            'isUnique' => [
                'rule' => 'isUnique',
                'last' => false,
                'message' => 'This username has already been taken.'
            ]
        ],
        'password' => [
            'notBlank' => [
                'rule' => 'notBlank',
                'message' => 'Please fill up password field.'
            ],
            'length' => [
                'rule' => [
                    'minLength',
                    '8'
                ],
                'last' => false,
                'required' => true,
                'message' => 'Password should have a minimum of 8 characters long'
            ],
            'whitespace' => [
                'rule' => '/^[^\s]*$/',
                'last' => false,
                'message' => 'No whitespace in password.'
            ]
        ],
        'password_confirmation' => [
            'notBlank' => [
                'rule' => 'notBlank',
                'message' => 'Please fill up confirm password field.'
            ],
            'password_confirm' => [
                'rule' => ['match_password'],
                'last' => false,
                'message' => 'Password confirmation does not match given password.',
            ]
        ],
        'old_password' => [
            'notBlank' => [
                'rule' => 'notBlank',
                'message' => 'Please fill up old password field.'
            ],
            'password_now' => [
                'rule' => 'password_verifies',
                'message' => 'Invalid current password.'
            ]
        ],
        'first_name' => [
            'notBlank' => [
                'rule' => 'notBlank',
                'message' => 'Please fill up first name field.'
            ],
            'alphaSpace' => [
                'rule' => '/^[a-zA-Z ]*$/',
                'required' => true,
                'message' => 'Letters and spaces only for name.'
            ]
        ],
        'last_name' => [
            'notBlank' => [
                'rule' => 'notBlank',
                'message' => 'Please fill up last name field.'
            ],
            'alphaSpace' => [
                'rule' => '/^[a-zA-Z ]*$/',
                'required' => true,
                'last' => false,
                'message' => 'Letters and spaces only for name.'
            ]
        ],
        'gender' => [
            'notBlank' => [
                'rule' => 'notBlank',
                'message' => 'Please fill up gender field.'
            ],
            'numeric' => [
                'rule' => 'numeric',
                'required' => true,
                'message' => 'Invalid gender input.'
            ],
            'range' => [
                'rule' => ['range', -1, 2],
                'message' => 'Invalid gender input.'
            ]
        ],
        'email' => [
            'notBlank' => [
                'rule' => 'notBlank',
                'message' => 'Please fill up email field.'
            ],
            'isEmail' => [
                'rule' => 'email',
                'required' => true,
                'last' => false,
                'message' => 'Please enter a valid email address.'
            ],
            'isUnique' => [
                'rule' => 'isUnique',
                'last' => false,
                'message' => 'This email has already been taken.'
            ]
        ],
        'mobile_no' => [
            'rule' => '/^$|^09\d{2}-?\d{3}-?\d{4}$/',
            'message' => 'Please enter a valid mobile number. <br> Ex. 09XX-XXX-XXXX'
        ],
        'birth_date' => [
            'notBlank' => [
                'rule' => 'notBlank',
                'message' => 'Please fill up birth date field.'
            ],
            'date' => [
                'rule' => 'date',
                'required' => true,
                'last' => false,
                'message' => 'Please enter a valid birth date.'
            ],
            'validBirthDate' => [
                'rule' => ['valid_birth_date'],
                'last' => false,
                'message' => 'Future dates are not allowed',
            ]
        ],
        'profile_picture' => [
            // 'uploadError' => [
            //     'rule' => ['uploadError', true],
            //     'message' => 'Image upload error.'
            // ],
            'fileSize' => [
                'rule' => ['isBelowMaxSize', 2048, false],
                'message' => 'File is larger than the maximum filesize'
            ],
            'completedUpload' => [
                'rule' => 'isCompletedUpload',
                'message' => 'File was not successfully uploaded'
            ],
            'extension' => [
                'rule' => [
                    'extension',
                    [
                        'jpeg',
                        'jpg',
                        'png',
                        'gif',
                        'bmp'
                    ]
                ],
                'required' => 'false',
                'allowEmpty' => true,
                'message' => 'Invalid image upload.'
            ],
            'mimeType' => [
                'rule' => [
                    'isValidMimeType',
                    [
                        'image/png',
                        'image/jpeg',
                        'image/gif',
                        'image/bmp'
                    ]
                ],
                'message' => 'Invalid image mime type.'
            ],
            'isUnderPhpSizeLimit' => [
                'rule' => 'isUnderPhpSizeLimit',
                'message' => 'File exceeds upload filesize limit'
            ]
        ]

    ];

    public function valid_birth_date($check)
    {
        if (time() <= strtotime($check['birth_date'])) {
            return false;
        }
        return true;
    }

    public function match_password($check, $confirm)
    {
        if ($this->data['User']['password'] !== $this->data['User']['password_confirmation']) {
            return false;
        }
        return true;
    }

    public function password_verifies()
    {
        $hashedPassword = Security::hash($this->data[$this->alias]['old_password'], 'blowfish', $this->field('password'));
        return $hashedPassword === $this->field('password');
    }

    public function beforeValidate($options = array())
    {
        if (empty($this->data[$this->alias]["profile_picture"]["name"])) {
            unset($this->data[$this->alias]["profile_picture"]);
            unset($this->data[$this->alias]["profile_picture_dir"]);
        }

        return true;
    }

    public function afterValidate()
    {
        if (isset($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = $this->passwordHash(
                $this->data[$this->alias]['password']
            );
        }

        return true;
    }

    private function passwordHash($password)
    {
        $passwordHasher = new BlowfishPasswordHasher();
        return $passwordHasher->hash($password);
    }
}
