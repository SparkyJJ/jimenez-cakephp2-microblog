<?php
App::uses('AppModel', 'Model');
class Comment extends AppModel
{
    public $actsAs = ['Containable'];

    public $belongsTo = [
        'Post' => [
            'className' => 'Post',
            'foreignKey' => 'post_id',
            'counterCache' => true
        ],
        'User' => [
            'className' => 'User',
            'foreignKey' => 'user_id',
            'fields' => [
                'id',
                'first_name',
                'last_name',
                'username',
                'profile_picture',
                'profile_picture_dir'
            ]
        ],
        'CommentCount' => [
            'className' => 'Comment',
            'foreignKey' => 'comment_id',
            'counterCache' => true,
            'counterScope' => ['Comment.deleted' => 0]
        ]
    ];

    public $hasMany = [
        'Reply' => [
            'className' => 'Comment',
            'foreignKey' => 'comment_id',
            'counterCache' => true,
            'dependent' => true
        ]
    ];

    var $validate = [
        'comment' => [
            'maxLength' => [
                'rule' => ['maxLength', 140],
                'message' => 'Post body must be no larger than 140 characters long.'
            ],
            'notBlank' => [
                'rule' => 'notBlank',
                'message' => 'Please fill up body field.'
            ]
        ]
    ];
}
