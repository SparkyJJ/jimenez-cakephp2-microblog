<?php
App::uses('AppModel', 'Model');
class Post extends AppModel
{
    public $belongsTo = [
        'User' => [
            'className' => 'User',
            'order' => ['Post.created' => 'desc'],
            'fields' => [
                'id',
                'username',
                'first_name',
                'last_name',
                'profile_picture',
                'profile_picture_dir'
            ]
        ],
        'Share_Post' => [
            'className' => 'Post',
            'foreignKey' => 'post_id',
            'counterCache' => true
        ],
        'Share_Post2' => [
            'className' => 'Post',
            'foreignKey' => 'post_id'
        ],
        'Share_User' => [
            'className' => 'User',
            'foreignKey' => 'user_share_id',
            'fields' => [
                'id',
                'username',
                'first_name',
                'last_name',
                'profile_picture',
                'profile_picture_dir'
            ]
        ]
    ];


    public $hasMany = array(
        'Like' => array(
            'className' => 'Like',
            'fields' => ['id', 'user_id', 'post_id']
        ),
        'Comment' => array(
            'className' => 'Comment',
            'fields' => ['id', 'user_id', 'comment', 'created']
        )
    );

    public $actsAs = [
        'Upload.Upload' => [
            'image' => [
                'fields' => [
                    'dir' => 'image_dir'
                ],
                'path' => '{ROOT}webroot{DS}img{DS}{model}{DS}{field}{DS}',
                'deleteOnUpdate' => true
            ]
        ],
        'Containable'
    ];


    public $validate = [
        'body' => [
            'maxLength' => [
                'rule' => array('maxLength', 140),
                'message' => 'Post body must be no larger than 140 characters long.'
            ],
            'notBlank' => [
                'rule' => 'notBlank',
                'message' => 'Please fill up body field.'
            ]
        ],
        'visibility' => [
            'numeric' => [
                'rule' => 'numeric',
                'required' => true,
                'last' => true,
                'message' => 'Invalid visibility.'
            ],
            'range' => [
                'rule' => ['range', -1, 3],
                'last' => true,
                'message' => 'Invalid visibility.'
            ],
            'notBlank' => [
                'rule' => 'notBlank',
                'message' => 'Please select a visibility.'
            ]
        ],
        'image' => [
            'extension' => [
                'rule' => [
                    'extension',
                    [
                        'jpeg',
                        'jpg',
                        'png',
                        'gif',
                        'bmp'
                    ]
                ],
                'required' => 'false',
                'allowEmpty' => true,
                'last' => true,
                'message' => 'Invalid image upload'
            ],
            'uploadError' => [
                'rule' => ['uploadError', true],
                'last' => true,
                'message' => 'Image upload error'
            ],
            'mimeType' => [
                'rule' => [
                    'mimeType',
                    [
                        'image/png',
                        'image/jpeg',
                        'image/jpeg',
                        'image/jpeg',
                        'image/gif',
                        'image/bmp'
                    ]
                ],
                'last' => true,
                'message' => 'Invalid image mime type'
            ],
            'fileSize' => [
                'rule' => [
                    'fileSize',
                    '<=',
                    '2MB'
                ],
                'message' => "Image should be less than or equalt to 2MB"
            ]
        ]

    ];

    public function beforeValidate($options = array())
    {
        if (empty($this->data[$this->alias]["image"]["name"])) {
            unset($this->data[$this->alias]["image"]);
            unset($this->data[$this->alias]["image_dir"]);
        }

        return true;
    }
}
