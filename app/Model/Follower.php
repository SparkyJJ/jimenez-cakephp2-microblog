<?php
App::uses('AppModel', 'Model');
class Follower extends AppModel
{
    public $actsAs = array('Containable');

    public $belongsTo = [
        'user_followee' => [
            'className' => 'User',
            'foreignKey' => 'user_follower_id',
            'counterCache' => 'following_count',
            'counterScope' => ['accepted' => 1]
        ],
        'user_follower' => [
            'className' => 'User',
            'foreignKey' => 'user_followee_id',
            'counterCache' => [
                'follower_count' => ['accepted' => 1],
                'follow_request_count' => ['accepted' => 0]
            ],
        ]
    ];
}
