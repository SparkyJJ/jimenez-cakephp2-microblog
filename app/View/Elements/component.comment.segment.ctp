<div class="ui segment comments-segment">
    <?php echo $this->element('component.comment.field', ['post' => $post]); ?>
    <?php echo $this->requestAction(['controller' => 'comments', 'action' => 'paginated_comments', $post['Post']['id']], array('return')); ?>
</div>