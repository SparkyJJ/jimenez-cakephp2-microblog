<?php echo $this->Form->create(
    'Comment',
    [
        'class' => 'ui form comment-field-form',
        'url' => [
            'controller' => 'comments',
            'action' => 'add',
            (isset($post['Post']['id'])) ? $post['Post']['id'] : null
        ],
        'inputDefaults' => [
            'label' => false,
            'div' => ['class' => 'field'],
            'errorMessage' => false
        ],
        'novalidate'
    ]
);
?>
<div class="ui raised segment rm-margin">
    <div class="ui inverted dimmer">
        <div class="ui loader"></div>
    </div>
    <div class="ui items rm-margin">
        <div class="item">
            <div class="ui circular image" style="max-width: 41px">
                <?php
                if (isset($user['User']['profile_picture'])) {
                    $dir = 'user/profile_picture/' . $user['User']['profile_picture_dir'] . '/sm_' . $user['User']['profile_picture'];
                } else {
                    $dir = '/img/user/profile_picture/default.png';
                }
                echo $this->Html->image(
                    $dir,
                    [
                        'class' => 'ui centered circular image',
                        'onerror' => 'this.onerror=null;this.src="/img/user/profile_picture/default.png";',
                        'url' => array('controller' => 'profile', 'action' => 'view', $user['User']['id'])
                    ]
                );
                ?>
            </div>
            <div class="middle aligned content" style="padding-left: 15px">
                <?php
                echo $this->Form->input(
                    'comment',
                    [
                        'placeholder' => 'Comment...',
                        'type' => 'textarea',
                        'rows' => 1,
                        'maxlength' => 140,
                        'div' => ['class' => 'field rm-margin'],
                    ]
                );
                ?>
            </div>
            <div style="padding-left: 15px">
                <button class="ui teal circular icon button">
                    <i class="paper plane icon"></i>
                </button>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?>