<div class="comment post-comment" data-comment-id="<?= $comment['Comment']['id'] ?>">
    <div class="avatar">
        <?php
        if (isset($comment['User']['profile_picture'])) {
            $dir = 'user/profile_picture/' . $comment['User']['profile_picture_dir'] . '/sm_' . $comment['User']['profile_picture'];
        } else {
            $dir = '/img/user/profile_picture/default.png';
        }
        echo $this->Html->image(
            $dir,
            [
                'class' => 'ui centered image',
                'onerror' => 'this.onerror=null;this.src="/img/user/profile_picture/default.png";',
                'url' => ['controller' => 'profile', 'action' => 'view', $comment['User']['id']]
            ]
        );
        ?>
    </div>
    <div class="content">
        <?php
        echo $this->Html->link(
            $comment['User']['first_name'] . ' ' . $comment['User']['last_name'],
            ['controller' => 'profile', 'action' => 'view', $comment['User']['id']],
            ['style' => 'color: teal', 'class' => 'author']
        );
        ?>
        <div class="metadata">
            @<?= h($comment['User']['username']) ?>
        </div>
        <div class="metadata">
            <span class="date">
                <?= date('F d, Y g:i A', strtotime($comment['Comment']['created'])) ?>
            </span>
        </div>
        <div class="text comment-text">
            <p><?= nl2br(h($comment['Comment']['comment'])) ?></p>
        </div>
        <div class="actions">
            <?php if (!isset($comment['Comment']['comment_id'])) : ?>
                <button class="ui toggle button reply comment rm-margin">Reply</button>
            <?php endif; ?>
            <?php if ($comment['Comment']['user_id'] === $user['User']['id']) : ?>
                <button class="ui toggle button edit comment rm-margin">Edit</button>
                <button class="ui toggle button delete comment rm-margin">Delete
                    <?php
                        echo $this->Form->postLink(
                            '',
                            ['controller' => 'comments', 'action' => 'delete', $comment['Comment']['id'], $comment['Comment']['post_id']]
                        );
                        ?>
                </button>
            <?php endif; ?>
        </div>
    </div>
    <?php if (!empty($comment['Reply'])) : ?>
        <div class="comments reply-comments">
            <?php
                foreach ($comment['Reply'] as $row) {
                    $row['Comment'] = $row;
                    $row['User'] = $row['Comment']['User'];
                    unset($row['Comment']['User']);
                    echo $this->element('item.comment', ['comment' => $row]);
                }
                ?>
        </div>
    <?php endif; ?>
</div>