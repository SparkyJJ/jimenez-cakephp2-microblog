<div class="ui raised teal fluid segment" style="margin: 0px">
  <?php if ($viewUser['isUser']) : ?>
    <a href="/profile/edit" class="ui teal right corner big label">
      <i class="settings icon"></i>
    </a>
  <?php endif; ?>
  <div class="ui fluid basic segment rm-margin">
    <div class="ui items">
      <div class="item">
        <div class="ui circular image" style="width:100px">
          <?php
          if (isset($viewUser['profile_picture'])) {
            $dir = 'user/profile_picture/' . $viewUser['profile_picture_dir'] . '/md_' . $viewUser['profile_picture'];
          } else {
            $dir = '/img/user/profile_picture/default.png';
          }
          echo $this->Html->image(
            $dir,
            [
              'onerror' => 'this.onerror=null;this.src="/img/user/profile_picture/default.png";'
            ]
          );
          ?>
        </div>
        <div class="middle aligned content">
          <div class="ui huge header">
            <?php
            echo $this->Html->link(
              h($viewUser['first_name'] . ' ' . $viewUser['last_name']),
              ['controller' => 'profile', 'action' => 'view', $viewUser['id']],
              ['style' => 'color: teal']
            );
            ?>
            <div class="sub header">@<?= h($viewUser['username']) ?></div>
          </div>
          <div class="description">
            <div class="ui large labels">
              <a href="/follow/followers/<?= h($viewUser['id']) ?>" class="ui basic teal label">
                Followers
                <div class="detail"><?= h($viewUser['follower_count']) ?></div>
              </a>
              <a href="/follow/following/<?= h($viewUser['id']) ?>" class="ui basic teal label">
                Following
                <div class="detail"><?= h($viewUser['following_count']) ?></div>
              </a>
              <?php if (!$viewUser['isUser']) {
                if ($viewUser['isFollowing']) {
                  if ($viewUser['isFollowing']['Follower']['accepted']) {
                    echo $this->Form->postLink(
                      'Unfollow',
                      ['controller' => 'followers', 'action' => 'unfollow', $viewUser['id'], $viewUser['username'], 0],
                      [
                        'class' => 'ui item teal label'
                      ]
                    );
                  } else {
                    echo $this->Form->postLink(
                      'Pending',
                      ['controller' => 'followers', 'action' => 'unfollow', $viewUser['id'], $viewUser['username'], 1],
                      [
                        'class' => 'ui item basic teal label'
                      ]
                    );
                  }
                } else {
                  echo $this->Form->postLink(
                    'Follow',
                    ['controller' => 'followers', 'action' => 'follow', $viewUser['id'], $viewUser['username']],
                    [
                      'class' => 'ui item teal label'
                    ]
                  );
                }
              }
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="ui accordion">
    <div id="main-title" class="title" style="padding: 0px">
      <div class="ui fluid center aligned container">
        <i class="centered aligned chevron down icon"></i>
      </div>
    </div>
    <div class="content">
      <div class="accordion transition visible" style="display: block !important;">
        <div class="ui title small header">
          <i class="dropdown icon"></i>
          Personal Information
        </div>
        <div class="content">
          <div class="ui grid container">
            <div class="row">
              <div class="eight wide column">Gender:
                <?php
                switch ($viewUser['gender']) {
                  case 0:
                    echo 'Male';
                    break;
                  case 1:
                    echo 'Female';
                    break;
                  case 2:
                    echo 'Other';
                    break;
                }
                ?>
              </div>
              <div class="eight wide column">Birth Date: <?= date('F j, Y', strtotime($viewUser['birth_date'])) ?></div>
            </div>
            <div class="row">
              <div class="eight wide column">Address: <?= h($viewUser['address']) ?></div>
            </div>
          </div>
        </div>
        <div class="ui title small header">
          <i class="dropdown icon"></i>
          Contact
        </div>
        <div class="content">
          <div class="ui grid container">
            <div class="row">
              <div class="eight wide column">Mobile: <?= h($viewUser['mobile_no']) ?></div>
              <div class="eight wide column">Email: <?= h($viewUser['email']) ?></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  $('.ui.accordion')
    .accordion();

  $('#main-title').click(function() {
    $("i.chevron").toggleClass("down up");
  })
</script>