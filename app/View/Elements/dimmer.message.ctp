<div class="ui active dimmer message">
    <div class="content">
        <h2 class="ui inverted icon header">
            <i class="<?php echo !empty($modal['icon']) ? $modal['icon'] : 'red exclamation circle icon'; ?>"></i>
            <div class="content">
                <?php echo !empty($modal['title']) ? $modal['title'] : 'Error'; ?>
                <div class="sub header">
                    <?php
                    if (is_array($modal['message'])) {
                        foreach ($modal['message'] as $row) {
                            foreach ($row as $value) {
                                echo h($value);
                                echo '<br>';
                            }
                        }
                    } else {
                        echo h($modal['message']);
                    }
                    ?>
                </div>
            </div>
        </h2>
        <div class="ui button" style="margin-top: 5px">Ok</div>
    </div>
</div>
<script>
    $('body').on('click','.ui.active.dimmer.message .ui.button', function() {
        $(this).closest('.ui.active.dimmer.message').remove()
    })
</script>