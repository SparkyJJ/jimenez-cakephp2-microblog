<h2 class="ui icon header">
    <i class="<?php echo !empty($modal['icon']) ? $modal['icon'] : 'red exclamation circle icon'; ?>"></i>
    <div class="content">
        <?php echo !empty($modal['title']) ? $modal['title'] : 'Error'; ?>
        <div class="sub header">
            <?php
            if (is_array($modal['message'])) {
                foreach ($modal['message'] as $row) {
                    foreach ($row as $value) {
                        echo h($value);
                        echo '<br>';
                    }
                }
            } else {
                echo h($modal['message']);
            }
            ?>
        </div>
    </div>
</h2>
<div class="actions">
    <div class="ui ok button">
        Ok
    </div>
</div>