<?php echo $this->Form->create(
    'Post',
    [
        'class' => 'ui form post-field-form',
        'url' => array(
            'controller' => 'posts',
            'action' => $postForm['action']
        ),
        'inputDefaults' => [
            'label' => false,
            'div' => ['class' => 'field'],
            'errorMessage' => false
        ],
        'type' => 'file',
        'novalidate'=> 'novalidate'
    ]
);
?>
<div class="ui raised segments post-field-form" style="margin: 0px">
    <div class="ui inverted dimmer post-form-loader">
        <div class="ui loader"></div>
    </div>
    <div class="ui teal segment">
        <h3><?= $postForm['title'] ?></h3>
    </div>
    <div class="ui gray segment scrolling content">
        <div style="margin-bottom: 20px">
            <?php
            echo $this->Form->input(
                'body',
                [
                    'placeholder' => 'Body',
                    'class' => 'post-field-body',
                    'style' => 'resize: none',
                    'type' => 'textarea',
                    'rows' => 2,
                    'maxlength' => 140,
                    'div' => ['class' => 'field rm-margin'],
                ]
            );
            ?>
            <div class="ui tiny indicating progress bodycount">
                <div class="bar"></div>
                <div class="label"></div>
            </div>
        </div>
        <div class="field">
            <label>Image Upload:</label>
            <?php
            echo $this->Form->file('image', ['class' => 'post-field-file']);
            echo $this->Form->input('image_dir', ['type' => 'hidden']);
            ?>
        </div>
        <div class="field preview">
            <?php if (isset($this->request->data['Post']['image'])) : ?>
                <label>Uploaded image:</label>
                <div class="ui segment" style="margin-top: 10px">
                    <?php
                        echo $this->Html->image(
                            'post/image/' . $this->request->data['Post']['image_dir'] . '/' . $this->request->data['Post']['image'],
                            [
                                'class' => 'ui centered rounded image',
                                'style' => 'max-height: 200px',
                                'id' => 'displayPostImage'
                            ]
                        );
                        ?>
                </div>
            <?php
            endif;
            ?>
        </div>
        <?php if (isset($postForm['share'])) : ?>
            <div class="field" id="share-post-segment">
                <?= $this->element('item.post', ['post' => $postForm['share']]); ?>
            </div>
        <?php endif; ?>
    </div>
    <div class="ui gray compact segment">
        <button class="ui button post-button">
            <?= $postForm['button'] ?>
        </button>
        <div class="ui visibility floating labeled icon dropdown button">
            <i class="eye icon"></i>
            <?php
            echo $this->Form->hidden(
                'visibility',
                [
                    'class' => 'text',
                ]
            );
            $this->Form->unlockField('Post.visibility');
            ?>
            <span class="text">Visibility</span>
            <div class="menu">
                <div class="header">
                    <i class="eye icon"></i>
                    Can be seen by
                </div>
            </div>
        </div>
    </div>
    <?php
    echo $this->Form->hidden(
        'user_share_id',
        [
            'class' => 'text',
        ]
    );
    echo $this->Form->hidden(
        'post_id',
        [
            'class' => 'text',
        ]
    );
    echo $this->Form->hidden(
        'type',
        [
            'class' => 'text',
            'value' => $postForm['type'],
        ]
    );
    ?>
</div>
<?php echo $this->Form->end();?>

<script>
    $('.bodycount').progress({
        value: <?= isset($this->request->data['Post']['body']) ? strlen($this->request->data['Post']['body']) : '0' ?>,
        total: 140,
        duration: 200,
        text: {
            active: '{value} / {total} characters'
        }
    });
    $('.ui.visibility.dropdown')
        .dropdown({
            values: [{
                    name: 'Public',
                    value: '0'
                },
                {
                    name: 'Followers',
                    value: '1',
                    selected: true
                },
                {
                    name: 'Only Me',
                    value: '2'
                }
            ]
        });

    <?php if (isset($this->request->data['Post']['visibility'])) : ?>
        $('.ui.visibility.dropdown').dropdown('set exactly', '<?= $this->request->data['Post']['visibility'] ?>');
    <?php endif; ?>


    $('.post-field-body').on('keyup', function() {
        $(this).parent().siblings('.bodycount').progress('set progress', this.value.length)
    })

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $(input).parent().siblings('.preview').html(
                    '<label>Preview: </label>' +
                    '<div class="ui segment" style="margin-top: 10px">' +
                    '<img src=' + e.target.result + ' class="ui centered rounded image" style="max-height: 200px">' +
                    '</div>'
                )
            }

            reader.readAsDataURL(input.files[0]);
        } else {
            $(input).parent().siblings('.preview').html('');
        }
    }

    $(".post-field-file").change(function() {
        readURL(this);
    });
</script>