<div class="ui grid">
    <div class="one wide column middle aligned">
        <?php
        if (!empty($post['Comment'])) {
            echo $this->Paginator->prev(
                '<i class="chevron left icon"></i>',
                [
                    'escape' => false,
                    'url' => ['controller' => 'comments', 'action' => 'paginated_comments'],
                    'class' => 'prev-comments',
                    'model' => 'Comment'
                ],
                null,
                array('class' => 'prev disabled')
            );
        }
        ?>
    </div>
    <div class="fourteen wide column">
        <div class="ui threaded comments <?= (!empty($post['Comment'])) ? 'comment-thread' : 'rm-margin' ?>">
            <?php
            foreach ($post['Comment'] as $row) {
                if (!isset($row['Comment'])) {
                    $row['Comment'] = $row;
                    $row['User'] = $row['Comment']['User'];
                    $row['Reply'] = $row['Comment']['Reply'];
                    unset($row['Comment']['Reply']);
                    unset($row['Comment']['User']);
                }
                echo $this->element('item.comment', ['comment' => $row]);
            }
            ?>
        </div>
    </div>
    <div class="one wide column middle aligned">
        <?php
        if (!empty($post['Comment'])) {
            echo $this->Paginator->next(
                '<i class="chevron right icon"></i>',
                [
                    'escape' => false,
                    'url' => ['controller' => 'comments', 'action' => 'paginated_comments'],
                    'class' => 'next-comments',
                    'model' => 'Comment'
                ],
                null,
                array('class' => 'next disabled')
            );
        }
        ?>
    </div>
</div>