<div class="ui tiny modal login">
    <div class="ui center aligned header blue huge">Log-in to your account</div>
    <div class="content">
        <?php echo $this->Form->create(
            'User',
            [
                'class' => 'ui large form',
                'url' => [
                    'controller' => 'home',
                    'action' => 'index'
                ],
                'inputDefaults' => [
                    'label' => false,
                    'div' => ['class' => 'field']
                ],
                'novalidate'
            ]
        );
        ?>
        <?php //echo $this->Flash->render('auth'); 
        ?>
        <?php echo $this->Form->input(
            'username',
            [
                'before' => '<div class="ui left icon input"><i class="user icon"></i>',
                'after' => '</div>',
                'placeholder' => 'Username'
            ]
        );
        echo $this->Form->input(
            'password',
            [
                'before' => '<div class="ui left icon input"><i class="lock icon"></i>',
                'after' => '</div>',
                'placeholder' => 'Password'
            ]
        );
        ?>
        <div class="ui error message"></div>
        <div class="ui divider"></div>
        <div class="actions">
            <?php
            echo $this->Form->button('Login <i class="checkmark icon"></i> ', [
                'type' => 'submit',
                'class' => 'ui primary right labeled icon button'
            ]);
            ?>
            <div class="ui black right floated cancel button">
                Cancel
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<script>
    $('.modal.login').on('click', '.ui.primary.right.labeled.icon.button', function() {
        $(this).addClass('disabled loading')
    })
</script>