<div class="user-card card" style="max-width: 200px" data-card-id='<?= $userCardData['id'] ?>'>
  <div class="ui centered image">
    <?php
    if (isset($userCardData['profile_picture'])) {
      $dir = 'user/profile_picture/' . $userCardData['profile_picture_dir'] . '/lg_' . $userCardData['profile_picture'];
    } else {
      $dir = '/img/user/profile_picture/default.png';
    }
    echo $this->Html->image(
      $dir,
      [
        'class' => 'ui small centered circular image',
        'onerror' => 'this.onerror=null;this.src="/img/user/profile_picture/default.png";'
      ]
    );
    ?>
  </div>
  <div class="content">
    <div class="header"><?= h($userCardData['first_name'] . ' ' . $userCardData['last_name']) ?></div>
    <div class="meta">
      <a>@<?= h($userCardData['username']) ?></a>
    </div>
    <div class="description">
    </div>
  </div>

  <div class="extra content">
    <?php if (isset($header) && $header === 'Pending') : ?>
      <div class="ui two tiny buttons">
        <?php
          echo $this->Form->postLink(
            'Accept',
            ['controller' => 'followers', 'action' => 'accept', $userCardData['id'], $userCardData['username']],
            [
              'class' => 'ui basic green button',
              'style' => 'color: inherit;text-decoration: none;',
              'escape' => false
            ]
          );
          ?>
        <?php
          echo $this->Form->postLink(
            'Decline',
            ['controller' => 'followers', 'action' => 'decline', $userCardData['id'], $userCardData['username']],
            [
              'class' => 'ui basic red button',
              'style' => 'color: inherit;text-decoration: none;',
              'escape' => false
            ]
          );
          ?>
      </div>
    <?php else : ?>
      <span>
        Joined in
        <?= date('Y', strtotime($userCardData['created'])) ?>
      </span>
    <?php endif; ?>
  </div>
</div>