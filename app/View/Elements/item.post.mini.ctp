<div class="card">
    <div class="ui items content">
        <div class="item">
            <div class="ui mini circular image">
                <?php
                echo $this->Html->image(
                    'user/profile_picture/' . $post['User']['profile_picture_dir'] . '/lg_' . $post['User']['profile_picture'],
                    [
                        'class' => 'ui centered circular image',
                        'onerror' => 'this.onerror=null;this.src="/img/user/profile_picture/default.png";',
                        'url' => array('controller' => 'profile', 'action' => 'view', $post['User']['id'])
                    ]
                );
                ?>
            </div>
            <div class="middle aligned content">
                <h4 class="ui teal header">
                    <?php
                    echo $this->Html->link(
                        $post['User']['first_name'] . ' ' . $post['User']['last_name'],
                        ['controller' => 'profile', 'action' => 'view', $post['User']['id']],
                        ['style' => 'color: teal']
                    );
                    ?>
                    <div class="sub header">@<?= $post['User']['username'] ?></div>
                </h4>
            </div>
        </div>
        <div class="description text-sm" style="word-wrap: break-word">
            <?= nl2br(h($post['Post']['body'])) ?>
        </div>
    </div>
    <div class="extra content">
    </div>
</div>