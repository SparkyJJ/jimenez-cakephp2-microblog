    <div class="ui stacked raised teal segment post-segment" data-post-id="<?= h($post['Post']['id']) ?>" data-user-id="<?= h($post['User']['id']) ?>" style="display: none">
        <?php if (!isset($post['isShare'])) : ?>
            <div class="ui top right attached label dropdown post-setting">
                <i class="ellipsis vertical icon large fitted"></i>
                <div class="menu">
                    <?php
                        echo $this->Html->link(
                            '<i class="eye icon"></i>View post',
                            ['controller' => 'posts', 'action' => 'view', h($post['Post']['id'])],
                            [
                                'class' => 'item',
                                'escape' => false
                            ]
                        );

                        if ($post['User']['id'] === $user['User']['id']) {
                            echo $this->Html->link(
                                '<i class="edit icon"></i>Edit',
                                ['controller' => 'posts', 'action' => 'edit', h($post['Post']['id'])],
                                [
                                    'class' => 'item',
                                    'escape' => false
                                ]
                            );
                            echo $this->Form->postLink(
                                '<i class="trash icon"></i>Delete',
                                ['controller' => 'posts', 'action' => 'delete', h($post['Post']['id'])],
                                [
                                    // 'confirm' => 'Are you sure?',
                                    'class' => 'sidemenu delete item',
                                    'escape' => false
                                ]
                            );
                        }
                        ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="ui items" style="margin: 0px!important">
            <div class="item">
                <div class="ui mini circular image">
                    <?php
                    if (isset($post['User']['profile_picture'])) {
                        $dir = 'user/profile_picture/' . $post['User']['profile_picture_dir'] . '/sm_' . $post['User']['profile_picture'];
                    } else {
                        $dir = '/img/user/profile_picture/default.png';
                    }
                    echo $this->Html->image(
                        $dir,
                        [
                            'class' => 'ui centered circular image',
                            'onerror' => 'this.onerror=null;this.src="/img/user/profile_picture/default.png";',
                            'url' => ['controller' => 'profile', 'action' => 'view', $post['User']['id']]
                        ]
                    );
                    ?>
                </div>
                <div class="middle aligned content">
                    <h3 class="ui teal header">
                        <?php
                        echo $this->Html->link(
                            $post['User']['first_name'] . ' ' . $post['User']['last_name'],
                            ['controller' => 'profile', 'action' => 'view', $post['User']['id']],
                            ['style' => 'color: teal']
                        );
                        ?>
                        <div class="sub header">
                            @<?= $post['User']['username'] ?>
                            <?php if (isset($post['Post']['visibility'])) : ?>
                                <span style="margin-left: 5px" data-tooltip="<?= $post['Post']['visibility'] === '0' ? 'Public' : ($post['Post']['visibility'] === '1' ? 'Followers' : 'Only Me') ?>" data-position="right center">
                                    <i class="small eye icon"></i>
                                </span>
                            <?php endif; ?>
                        </div>
                    </h3>
                </div>
            </div>
            <div class="content">
                <?php if (!$post['Post']['deleted'] && isset($post['Post']['id'])) : ?>
                    <div class="description text-md">
                        <?= nl2br(h($post['Post']['body'])) ?>
                        <?php if (isset($post['Post']['image'])) : ?>
                            <div class="ui segment" style="margin-top: 10px">
                                <?php
                                        echo $this->Html->image(
                                            'post/image/' . $post['Post']['image_dir'] . '/' . $post['Post']['image'],
                                            [
                                                'class' => 'ui centered rounded image',
                                                'style' => 'max-height: 200px'
                                            ]
                                        );
                                        ?>
                            </div>
                        <?php endif; ?>
                        <?php
                            if (isset($post['Share_User']['id'])) {
                                $sharePost['Post'] = $post['Share_Post'];
                                $sharePost['User'] = $post['Share_User'];
                                if (isset($post['Share_Post']['Share_User']['id'])) {
                                    if (isset($post['Share_Post']['Share_Post']['id'])) {
                                        $sharePost['Share_Post'] = $post['Share_Post']['Share_Post'];
                                    } else {
                                        $sharePost['Share_Post'] = null;
                                    }
                                    $sharePost['Share_User'] = $post['Share_Post']['Share_User'];
                                }
                                $sharePost['isShare'] = true;
                                echo '<div class="ui basic segment">';
                                echo $this->element('item.post', ['post' => $sharePost]);
                                echo '</div>';
                            }
                            ?>

                    </div>
                    <div class="extra" style="color: grey; margin-top: 10px">
                        <p>
                            <?= date('F d, Y g:i A', strtotime($post['Post']['created'])); ?>
                            <?php if (strtotime($post['Post']['created']) != strtotime($post['Post']['modified'])) : ?>
                                <span style="margin-left: 5px" data-tooltip="Edited: <?= date('F d, Y g:i A', strtotime($post['Post']['modified'])) ?>" data-position="right center">
                                    <i class="small info circle icon"></i>
                                </span>
                            <?php endif; ?>
                        </p>
                        <?php if (!isset($post['isShare'])) : ?>
                            <div class="ui mini labeled button post-shared-item" tabindex="0">
                                <div class="ui mini compact teal button like-post <?= (empty($post['Like'])) ? '' : 'basic' ?>" data-state="<?= (empty($post['Like'])) ? 'like' : 'unlike' ?>">
                                    <?php
                                            echo $this->Form->postLink(
                                                '<i class="thumbs up icon"></i><span class="like-content">' . ((empty($post['Like'])) ? 'Like' : 'Unlike') . '</span>',
                                                ['controller' => 'likes', 'action' => (empty($post['Like'])) ? 'like' : 'unlike', h($post['Post']['id'])],
                                                [
                                                    'escape' => false,
                                                    'style' => 'color: inherit; text-decoration: none;',
                                                    'class' => 'like-anchor'
                                                ]
                                            );
                                            ?>
                                </div>
                                <a class="ui basic teal left pointing label like-count">
                                    <?= h($post['Post']['like_count']) ?>
                                </a>
                            </div>
                            <div class="ui mini labeled button post-shared-item" tabindex="0">
                                <div class="ui mini compact teal button comment-post <?= $post['Post']['comment_count'] > 0 ? 'basic' : '' ?>">
                                    <i class="comments icon"></i> Comments
                                </div>
                                <a class="ui basic left pointing teal label comment-count">
                                    <?= h($post['Post']['comment_count']) ?>
                                </a>
                            </div>
                            <div class="ui mini labeled button post-shared-item" tabindex="0">
                                <div class="ui mini compact teal button share-post">
                                    <i class="share icon"></i> Share
                                </div>
                                <a class="ui basic left pointing teal label">
                                    <?= $post['Post']['post_count'] ?>
                                </a>
                            </div>
                            <div class="ui segment comments-segment" <?= $post['Post']['comment_count'] <= 0 ? 'style="display: none"' : '' ?>>
                                <?php echo $this->element('component.comment.field', ['post' => $post]); ?>
                                <?php echo $this->requestAction(['controller' => 'comments', 'action' => 'paginated_comments', $post['Post']['id']], ['return']); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php else : ?>
                    <?php echo $this->element('message.error', ['errors' => ['header' => 'Unavailable Content. Unable to see post.']]); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>