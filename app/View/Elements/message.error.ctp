<div class="ui error message">
    <div class="header">
        <?= $errors['header'] ?>
    </div>
    <?php if(isset($errors['list'])): ?>
    <ul class="list">
        <?php
        foreach ($errors['list'] as $row) {
            foreach ($row as $value) {
                echo '<li>';
                echo __($value);
                echo '</li>';
            }
        }
        ?>
    </ul>
    <?php endif; ?>
</div>