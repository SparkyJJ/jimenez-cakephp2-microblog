<div class="ui tiny modal flash-modal <?php echo !empty($params['class']) ? $params['class'] : ''; ?>" id="<?php echo $key; ?>Message">
    <h2 class="ui icon header">
        <i class="<?php echo !empty($params['icon']) ? $params['icon'] : 'red exclamation circle icon'; ?>"></i>
        <div class="content">
            <?php echo !empty($params['title']) ? $params['title'] : 'Error'; ?>
            <div class="sub header">
                <?php
                if(is_array($message)) {
                    foreach($message as $row) {
                        foreach($row as $value) {
                            echo __($value);
                            echo '<br>';
                        }
                    }
                } else {
                    echo __($message);
                }
                ?>
            </div>
        </div>
    </h2>
    <div class="actions">
        <div class="ui ok button">
            Ok
        </div>
    </div>
</div>

<script>
    $('.ui.modal.flash-modal')
        .modal('show');
</script>