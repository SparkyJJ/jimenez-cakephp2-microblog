<div class="ui teal fixed stackable top menu" style="z-index: 999">
    <a href="/main" class="active item">
        <h3>SparkBlog</h3>
    </a>
    <div class="right menu">
        <div class="teal item">
            <?php
            echo $this->Form->create(
                'Search',
                [
                    // 'class' => 'ui large form',
                    'url' => array(
                        'controller' => 'search',
                        'action' => 'index'
                    ),
                    'inputDefaults' => [
                        'label' => false,
                        'div' => ['class' => 'ui transparent icon input'],
                        'errorMessage' => false
                    ]
                ]
            );

            echo $this->Form->input(
                'search_item',
                array(
                    'after' => '<i class="search link icon"></i>',
                    'placeholder' => 'Search...'
                )
            );
            echo $this->Form->end(); 
            ?>
        </div>
        <div class="ui item">
            <a href="/logout" class="ui compact right labeled icon button">
                <i class="sign-out icon"></i>
                Logout
            </a>
        </div>
    </div>
</div>