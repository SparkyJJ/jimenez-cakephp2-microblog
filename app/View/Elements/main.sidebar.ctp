<div class="ui teal sticky fluid vertical menu" style="box-shadow: 0px 0px 10px #ddd">
    <a href="/profile/view" class="item">
        <?php
        if (isset($user['User']['profile_picture'])) {
            $dir = 'user/profile_picture/' . $user['User']['profile_picture_dir'] . '/lg_' . $user['User']['profile_picture'];
        } else {
            $dir = '/img/user/profile_picture/default.png';
        }
        echo $this->Html->image(
            $dir,
            [
                'class' => 'ui small centered circular image',
                'onerror' => 'this.onerror=null;this.src="/img/user/profile_picture/default.png";'
            ]
        );
        ?>
        <h3 class="ui center aligned header" style="margin-top: 15px">
            <?= $user['User']['first_name'] . ' ' . $user['User']['last_name'] ?>
            <div class="sub header">@<?= $user['User']['username'] ?></div>
        </h3>
    </a>
    <a href="/main" class="item" style="border-top: 2px solid teal">
        Home
    </a>
    <a href="/profile/view" class="item">
        Profile
    </a>
    <a href="/follow/followers/<?= $user['User']['id'] ?>" class="item">
        Followers
        <div class="ui teal label"><?= $user['User']['follower_count'] ?></div>
    </a>
    <a href="/follow/following/<?= $user['User']['id'] ?>" class="item">
        Following
        <div class="ui teal label"><?= $user['User']['following_count'] ?></div>
    </a>
    <a href="/follow/requests" class="item">
        Requests
        <div class="ui teal label"><?= $user['User']['follow_request_count'] ?></div>
    </a>
    <!-- <a href="/notification" class="item">
        Notifications
    </a> -->
    <a href="/profile/edit" class="item">
        Account Settings
    </a>
    <a href="/logout" class="item">
        Log Out
    </a>
</div>