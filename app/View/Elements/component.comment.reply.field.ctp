<div class="ui segment <?= $content['class'] ?>-field">
    <div class="ui inverted dimmer">
        <div class="ui loader"></div>
    </div>
    <?php echo $this->Form->create(
        'Comment',
        [
            'class' => 'ui form ' . $content['class'] . '-comment-field-form',
            'url' => [
                'controller' => 'comments',
                'action' => $content['type'],
                $content['post_id'],
                $content['comment_id'],
            ],
            'inputDefaults' => [
                'label' => false,
                'div' => ['class' => 'field'],
                'errorMessage' => false
            ],
            'novalidate'
        ]
    );
    ?>
    <div class="fields rm-margin">
        <div class="fourteen wide field">
            <?php
            echo $this->Form->input(
                'comment',
                [
                    'placeholder' => $content['placeholder'],
                    'type' => 'textarea',
                    'rows' => 1,
                    'maxlength' => 140,
                    'div' => ['class' => 'field rm-margin'],
                    'value' => isset($content['comment']) ? $content['comment'] : null
                ]
            );
            echo $this->Form->hidden('id');
            echo $this->Form->hidden('comment_id');
            echo $this->Form->hidden('post_id_id');
            ?>
        </div>
        <div class="one wide field">
            <button class="ui teal circular icon button">
                <i class="paper plane icon"></i>
            </button>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>