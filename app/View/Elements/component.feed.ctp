<div class="ui basic segment feed-segment rm-padding">
    <div class="ui inverted dimmer main-dimmer">
        <div class="ui loader"></div>
    </div>
    <?php
    foreach ($posts as $post) {
        echo $this->element(
            'item.post',
            [
                'post' => $post
            ]
        );
    }

    echo $this->Paginator->numbers([
        'before' => '<div class="ui basic center aligned segment"><div class="ui main pagination menu">',
        'url' => ['controller' => 'feed', 'action' => 'view'],
        'after' => '</div></div>',
        'class' => 'item',
        'tag' => 'span',
        'separator' => false,
        'ellipsis' => false,
        'currentClass' => 'active',
        'model' => 'Post'
    ]);
    ?>
</div>