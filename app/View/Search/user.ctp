<h2 class="ui header">
    <i class="search icon"></i>
    <div class="content">
        Search Results for: "<?= $headerItem ?>"
    </div>
</h2>
<div class="ui raised segments">
    <div class="ui teal segment">
        <h3>Users</h3>
    </div>
    <div class="ui gray segment link cards">
        <?php if (isset($users) && !empty($users)) : ?>
            <?php
                foreach ($users as $row) {
                    echo $this->element('item.user', ['userCardData' => $row['User']]);
                }
                ?>
        <?php else : ?>
            <h4 class="ui center aligned icon header">
                No users found
            </h4>
        <?php endif; ?>
    </div>
    <?php
    echo $this->Paginator->numbers([
        'before' => '<div class="ui basic center aligned segment"><div class="ui search pagination menu">',
        'after' => '</div></div>',
        'class' => 'item',
        'tag' => 'span',
        'separator' => false,
        'ellipsis' => false,
        'currentClass' => 'active',
        'model' => 'User'
    ]);
    ?>
</div>