<h2 class="ui header">
    <i class="search icon"></i>
    <div class="content">
        Search Results for: "<?= h($headerItem) ?>"
    </div>
</h2>
<div class="ui raised segments">
    <div class="ui teal segment">
        <h3>Users</h3>
    </div>
    <div class="ui gray segment link cards">
        <?php if (isset($users) && !empty($users)) : ?>
            <?php
                foreach ($users as $row) {
                    echo $this->element('item.user', ['userCardData' => $row['User']]);
                }
                ?>
        <?php else : ?>
            <h4 class="ui center aligned icon header">
                No users found
            </h4>
        <?php endif; ?>
    </div>
    <?php
    echo $this->Form->create(
        'Search',
        [
            'class' => 'search-sub-form',
            'url' => [
                'controller' => 'search',
                'action' => 'user'
            ]
        ]
    );
    echo $this->Form->hidden('search_item');
    ?>
    <div class="ui search bottom attached button" tabindex="0">See more Users</div>
    <?php
    echo $this->Form->end();
    ?>
</div>


<div class="ui raised segments">
    <div class="ui teal segment">
        <h3>Posts</h3>
    </div>
    <div class="ui gray segment">
        <?php
        if (isset($posts) && !empty($posts)) : ?>
            <?php
                foreach ($posts as $row) {
                    echo $this->element('item.post', ['post' => $row]);
                }
                ?>
        <?php else : ?>
            <h4 class="ui center aligned icon header">
                No posts found
            </h4>
        <?php endif; ?>
    </div>
    <?php
    echo $this->Form->create(
        'Search',
        [
            'class' => 'search-sub-form',
            'url' => [
                'controller' => 'search',
                'action' => 'post'
            ]
        ]
    );
    echo $this->Form->hidden('search_item');
    ?>
    <div class="ui search bottom attached button" tabindex="0">See more Posts</div>
    <?php
    echo $this->Form->end();
    ?>
</div>