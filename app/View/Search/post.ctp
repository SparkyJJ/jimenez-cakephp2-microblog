<h2 class="ui header">
    <i class="search icon"></i>
    <div class="content">
        Search Results for: "<?= $headerItem ?>"
    </div>
</h2>
<div class="ui raised segments">
    <div class="ui inverted dimmer main-dimmer">
        <div class="ui loader"></div>
    </div>
    <div class="ui teal segment">
        <h3>Posts</h3>
    </div>
    <div class="ui gray segment">
        <?php if (isset($posts) && !empty($posts)) : ?>
            <?php
                foreach ($posts as $row) {
                    echo $this->element('item.post', ['post' => $row]);
                }
                ?>
        <?php else : ?>
            <h4 class="ui center aligned icon header">
                No posts found
            </h4>
        <?php endif; ?>
        <?php
        echo $this->Paginator->numbers([
            'before' => '<div class="ui basic center aligned segment"><div class="ui search pagination menu">',
            'after' => '</div></div>',
            'class' => 'item',
            'tag' => 'span',
            'separator' => false,
            'ellipsis' => false,
            'currentClass' => 'active',
            'model' => 'Post'
        ]);
        ?>
    </div>
</div>