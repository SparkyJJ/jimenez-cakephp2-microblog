<!DOCTYPE html>
<html>

<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
	echo $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0, maximum-scale=1.0'));

	// echo $this->Html->css('font.css');
	// echo $this->Html->css('semantic.min.css');
	echo $this->Html->css('semantic-main.min.css');
	echo $this->Html->css('main.css');
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('semantic/accordion.min.js');
	echo $this->Html->script('semantic/visibility.min.js');
	echo $this->Html->script('semantic/transition.min.js');
	echo $this->Html->script('semantic/dimmer.min.js');
	echo $this->Html->script('semantic/modal.min.js');
	echo $this->Html->script('semantic/dropdown.min.js');
	echo $this->Html->script('semantic/form.min.js');
	echo $this->Html->script('semantic/progress.min.js');
	echo $this->Html->script('semantic/sidebar.min.js');
	echo $this->Html->script('semantic/sticky.min.js');
	// echo $this->Html->script('../css/semantic-ui/semantic.min.js');

	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->fetch('script');
	?>
</head>

<body style="background-color: darkgray">
	<?php echo $this->fetch('content'); ?>
	<?php echo $this->Flash->render(); ?>
	<?= $this->Html->script('main.js'); ?>
	<?= $this->Html->script('comments.js'); ?>
</body>

</html>