<!DOCTYPE html>
<html>

<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
	echo $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0, maximum-scale=1.0'));
	
	echo $this->Html->css('semantic-home.min.css');
	echo $this->Html->css('home.min.css');
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('semantic/visibility.min.js');
	echo $this->Html->script('semantic/transition.min.js');
	echo $this->Html->script('semantic/dimmer.min.js');
	echo $this->Html->script('semantic/modal.min.js');

	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->fetch('script');
	?>
</head>

<body>
	<div class="pusher">
		<div class="ui inverted vertical masthead segment">
			<div class="ui container">
				<div class="ui inverted secondary large pointing menu">
					<a class="<?= $home ?> item" href="/">Home</a>
					<a class="<?= $register ?> item" href="/register">Register</a>
				</div>
			</div>
			<?php echo $this->fetch('content'); ?>
		</div>
	</div>
	
	<?php echo $this->Flash->render(); ?>
	<?php //echo $this->element('sql_dump'); 
	?>
</body>
<script>
$('body').on('click','#reactivate', function() {
	$(this).replaceWith('<br><i class="mini notched circle loading icon"></i>')
	$('.ui.ok.button').replaceWith('&nbsp;')
})
</script>
</html>