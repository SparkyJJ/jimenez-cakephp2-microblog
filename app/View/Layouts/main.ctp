<!DOCTYPE html>
<html>

<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo $this->fetch('title'); ?>
    </title>
    <?php
    echo $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0, maximum-scale=1.0'));
    $this->response->disableCache();

    // header("Cache-Control: no-cache, no-store, must-revalidate");
    // header("Pragma: no-cache");
    // header("Expires: 0");

    echo $this->Html->css('semantic-main.min.css');
    echo $this->Html->css('main.css');
    echo $this->Html->script('jquery.min.js');
    echo $this->Html->script('semantic/accordion.min.js');
    echo $this->Html->script('semantic/visibility.min.js');
    echo $this->Html->script('semantic/transition.min.js');
    echo $this->Html->script('semantic/dimmer.min.js');
    echo $this->Html->script('semantic/modal.min.js');
    echo $this->Html->script('semantic/dropdown.min.js');
    echo $this->Html->script('semantic/form.min.js');
    echo $this->Html->script('semantic/progress.min.js');
    echo $this->Html->script('semantic/sidebar.min.js');
    echo $this->Html->script('semantic/sticky.min.js');

    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
    ?>

    <script>
        $(document)
            .ready(function() {

                // fix menu when passed
                $('.masthead')
                    .visibility({
                        once: false,
                        onBottomPassed: function() {
                            $('.fixed.menu').transition('fade in');
                        },
                        onBottomPassedReverse: function() {
                            $('.fixed.menu').transition('fade out');
                        }
                    });

                // create sidebar and attach to menu open
                $('.ui.sidebar')
                    .sidebar('attach events', '.toc.item');

                $('.ui.sticky')
                    .sticky({
                        context: '#main-segment',
                        offset: 80,
                        pushing: false
                    });

            });
    </script>
</head>

<body>
    <?php echo $this->element('component.navbar'); ?>
    <div class="ui fluid container" style="padding: 80px 180px 40px">
        <div class="ui main-segment basic segment" id="main-segment" style="margin-left:300px;padding:0px; min-width:300px; min-height:600px">
            <div class="ui left dividing rail">
                <?php echo $this->element('main.sidebar'); ?>
            </div>
            <?php echo $this->fetch('content'); ?>
        </div>
    </div>

    <?php echo $this->Flash->render(); ?>
    <div class="ui general small modal">
        <div class="scrolling content">
            <p>Very long content goes here</p>
        </div>
    </div>

    <?php //echo $this->element('sql_dump'); 
    ?>
    <?= $this->Html->script('main.js'); ?>
    <?= $this->Html->script('comments.js'); ?>
    <?= $this->Html->script('post.js'); ?>
</body>

</html>