<!DOCTYPE html>
<html>

<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo $this->fetch('title'); ?>
    </title>
    <?php
    echo $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0, maximum-scale=1.0'));

    echo $this->Html->css('semantic/semantic.min.css');
    echo $this->Html->css('calendar.min.css');
    echo $this->Html->css('home.css');
    echo $this->Html->script('jquery.min.js');
    echo $this->Html->script('../css/semantic/semantic.min.js');
    echo $this->Html->script('calendar.min.js');

    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
    ?>

</head>

<body>
    <?php echo $this->fetch('content'); ?>
</body>

</html>