<!DOCTYPE html>
<html>

<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
	echo $this->Html->meta('icon');

	echo $this->Html->css('semantic-ui/semantic.min.css');
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('../css/semantic-ui/semantic.min.js');

	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->fetch('script');
	?>
</head>

<body>
	<div id="content">
		<?php echo $this->Flash->render(); ?>

		<?php echo $this->fetch('content'); ?>
	</div>
	<?php //echo $this->element('sql_dump'); 
	?>
</body>

</html>