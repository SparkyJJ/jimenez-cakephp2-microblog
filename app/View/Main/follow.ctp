<?php
echo $this->element(
    'component.profile',
    [
        'viewUser' => $viewUserData
    ]
);
?>
<h4 class="ui grey horizontal divider header">
    <i class="users icon"></i>
    <?= $header ?>
</h4>
<?php if (!empty($follows)) : ?>
    <div class="ui basic segment">
        <div class="ui link cards">
            <?php
                foreach ($follows as $row) {
                    echo $this->element(
                        'item.user',
                        [
                            'userCardData' => $row["user_$userType"]
                        ]
                    );
                }
                ?>

        </div>
    </div>
<?php else : ?>
    <h4 class="ui grey horizontal divider header" style="margin-top:100px">
        <i class="frown outline icon"></i>
        Nothing to show
    </h4>
<?php endif; ?>

<?php
echo $this->Paginator->numbers([
    'before' => '<div class="ui basic center aligned segment"><div class="ui pagination menu">',
    'after' => '</div></div>',
    'class' => 'item',
    'tag' => 'span',
    'separator' => false,
    'ellipsis' => false,
    'currentClass' => 'active'
]);
?>