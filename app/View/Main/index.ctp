<?php echo $this->Session->flash('modalFeed'); ?>

<?php echo $this->element('component.post.field'); ?>
<?php for ($i = 0; $i < 5; $i++) : ?>
    <div class="ui segment post-loading">
        <div class="ui placeholder">
            <div class="image header">
                <div class="line"></div>
                <div class="line"></div>
            </div>
            <div class="paragraph">
                <div class="line"></div>
                <div class="line"></div>
                <div class="line"></div>
                <div class="line"></div>
                <div class="line"></div>
            </div>
        </div>
    </div>
<?php endfor; ?>
<?php echo $this->element('component.feed', ['posts' => $posts])?>