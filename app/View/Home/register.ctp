<?php
echo $this->Html->css('semantic/popup.min.css', ['inline' => false]);
echo $this->Html->css('calendar.min.css', ['inline' => false]);
echo $this->Html->script('semantic/popup.min.js', ['inline' => false]);
echo $this->Html->script('calendar.min.js', ['inline' => false]);
?>
<div class="ui stacked very padded text green container segment">
    <div class="ui inverted dimmer">
        <div class="ui medium loader"></div>
    </div>
    <div class="ui center aligned header green large">Create a new account!</div>
    <?php
    if (isset($errors)) {
        // echo $this->element('message.error');
    }
    ?>
    <?php echo $this->Form->create(
        'User',
        [
            'class' => 'ui form',
            'url' => [
                'controller' => 'home',
                'action' => 'register'
            ],
            'inputDefaults' => [
                'label' => false,
                'div' => ['class' => 'field'],
                'before' => '<div class="ui left icon input">',
                'after' => '</div>',
                // 'errorMessage' => false
                'error' => [
                    'attributes' => ['wrap' => 'div', 'class' => 'ui pointing red basic label']
                ]
            ],
            'type' => 'file',
            'novalidate' => 'novalidate'
        ]
    );
    ?>
    <?php //echo $this->Flash->render('auth'); ?>
    <?php echo $this->Form->input(
        'username',
        [
            'between' => '<i class="user icon"></i>',
            'placeholder' => 'Username'
        ]
    );
    echo $this->Form->input(
        'password',
        [
            'between' => '<i class="lock icon"></i>',
            'placeholder' => 'Password'
        ]
    );
    echo $this->Form->input(
        'password_confirmation',
        [
            'required' => true,
            'between' => '<i class="unlock alternate icon"></i>',
            'placeholder' => 'Confirm Password',
            'type' => 'password'
        ]
    );
    ?>

    <div class="ui divider"></div>
    <div class="field">
        <label>Name:</label>
        <div class="two fields">
            <?php
            echo $this->Form->input(
                'first_name',
                [
                    'between' => '<i class="user icon"></i>',
                    'placeholder' => 'First Name'
                ]
            );
            echo $this->Form->input(
                'last_name',
                [
                    'between' => '<i class="user icon"></i>',
                    'placeholder' => 'Last Name'
                ]
            );
            ?>
        </div>
    </div>
    <div class="two fields">
        <div class="field">
            <div class="ui segment inline fields gender <?= (isset($errors['gender'])) ? 'error' : '' ?>">
                <label>Gender:</label>
                <div class="field">
                    <div class="ui radio checkbox">
                        <?php
                        echo $this->Form->radio(
                            'gender',
                            [
                                '0' => '&nbsp;Male', '1' => '&nbsp;Female'
                            ],
                            [
                                'escape' => false,
                                'between' => '<br>',
                                'legend' => false,
                                'type' => 'radio',
                                'separator' => '</div></div><div class="field"><div class="ui radio checkbox">'
                            ]
                        );
                        ?>
                    </div>
                </div>
            </div>
            <?php if (isset($errors['gender'])) : ?>
                <div class="ui pointing red basic label">
                    <?php if (count($errors['gender']) > 1) : ?>
                        <ul>
                            <?php
                                    foreach ($errors['gender'] as $value) {
                                        echo "<li>$value</li>";
                                    }
                                    ?>
                        </ul>
                    <?php else : ?>
                        <?php echo $errors['gender'][0]; ?>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>
        <?php
        echo $this->Form->input(
            'address',
            [
                'between' => '<i class="home icon"></i>',
                'placeholder' => 'Address',
                'div' => ['class' => 'field'],
            ]
        );
        ?>
    </div>
    <div class="two fields">
        <div class="field <?= isset($errors['birth_date']) ? 'error' : '' ?>">
            <div class="ui calendar" id="birthdate">
                <?php
                echo $this->Form->input(
                    'birth_date',
                    [
                        'type' => 'text',
                        'div' => false,
                        'between' => '<i class="calendar icon"></i>',
                        'placeholder' => 'Birth Date',
                        'autocomplete' => "off"
                    ]
                );
                ?>
            </div>
        </div>
        <?php
        echo $this->Form->input(
            'mobile_no',
            [
                'between' => '<i class="phone icon"></i>',
                'placeholder' => 'Mobile Number (Ex. 09XX-XXX-XXXX)',
                'error' => [
                    'attributes' => [
                        'escape' => false,
                        'wrap' => 'div',
                        'class' => 'ui pointing red basic label'
                    ]
                ]
            ]
        );
        ?>
    </div>
    <?php
    echo $this->Form->input(
        'email',
        [
            'between' => '<i class="mail icon"></i>',
            'placeholder' => 'Email'
        ]
    );
    ?>
    <div class="field">
        <label>Image Upload:</label>
        <?php
        echo $this->Form->file('profile_picture');
        echo $this->Form->input('profile_picture_dir', ['type' => 'hidden']);
        ?>
    </div>
    <div class="ui error message"></div>
    <div class="ui divider"></div>
    <div class="ui basic center aligned segment">
        <?php
        echo $this->Form->button('Register <i class="checkmark icon"></i> ', [
            'type' => 'submit',
            'class' => 'ui positive right labeled icon button huge'
        ]);
        ?>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<script>
    $('.ui.form').submit(function() {
        $('.dimmer').addClass('active');
    });

    $('#birthdate').calendar({
        type: 'date',
        formatter: {
            date: function(date, settings) {
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth() + 1;
                var year = date.getFullYear();
                return year + '-' + month + '-' + day;
            }
        }
    });
</script>