<div class="ui text center aligned container">
    <h1 class="ui inverted header">
        &nbsp;<span class="mbtitle" style="display: none">SparkBlog</span>&nbsp;
    </h1>
    <h2>&nbsp;<span class="mbtitle" style="display: none">Do whatever you want with it.</span>&nbsp;</h2>
    <div class="ui huge buttons">
        <div id="button-login" class="ui primary button">Login</i></div>
        <div class="or"></div>
        <div id="button-register" class="ui positive button">Register</i></div>
    </div>
</div>
<?php echo $this->element('modal.login'); ?>

<script>
    $(document).ready(function() {
        $('#button-login').click(function() {
            $('.ui.modal.login').modal('show')
        })

        $('#button-register').click(function() {
            window.location.href = '/register'
        })

        $('.mbtitle').transition('fade down', {
            duration: 2000
        })
    });
</script>