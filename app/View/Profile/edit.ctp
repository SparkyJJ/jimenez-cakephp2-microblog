<?php
echo $this->Html->css('semantic/popup.min.css', ['inline' => false]);
echo $this->Html->css('calendar.min.css', ['inline' => false]);
echo $this->Html->script('semantic/popup.min.js', ['inline' => false]);
echo $this->Html->script('calendar.min.js', ['inline' => false]);
?>
<?php
// if (isset($errors)) {
//     echo $this->element('message.error');
// }
?>
<div class="ui raised teal segment rm-margin">
    <div class="ui center aligned header teal icon">
        <i class="settings icon"></i>
        Account Settings
    </div>
    <?php echo $this->Form->create(
        'User',
        [
            'class' => 'ui large form',
            'url' => [
                'controller' => 'profile',
                'action' => 'edit'
            ],
            'inputDefaults' => [
                'label' => false,
                'div' => ['class' => 'field'],
                'before' => '<div class="ui left icon input">',
                'after' => '</div>',
                // 'errorMessage' => false
                'error' => [
                    'attributes' => ['wrap' => 'div', 'class' => 'ui pointing red basic label']
                ]
            ],
            'type' => 'file',
            'novalidate' => 'novalidate'
        ]
    );
    ?>
    <div class="ui divider"></div>
    <div class="field">
        <label>Name:</label>
        <div class="two fields">
            <?php
            echo $this->Form->input(
                'first_name',
                [
                    'between' => '<i class="user icon"></i>',
                    'placeholder' => 'First Name'
                ]
            );
            echo $this->Form->input(
                'last_name',
                [
                    'between' => '<i class="user icon"></i>',
                    'placeholder' => 'Last Name'
                ]
            );
            ?>
        </div>
    </div>
    <div class="inline fields">
        <label>Gender:</label>
        <div class="field">
            <div class="ui radio checkbox">
                <?php
                echo $this->Form->radio(
                    'gender',
                    [
                        '0' => 'Male', '1' => 'Female'
                    ],
                    [
                        'between' => '<br>',
                        'legend' => false,
                        'type' => 'radio',
                        'separator' => '</div>
                    </div>
                    <div class="field">
                      <div class="ui radio checkbox">'
                    ]
                );
                ?>
            </div>
        </div>
    </div>
    <div class="two fields">
        <div class="field <?= isset($errors['birth_date']) ? 'error' : '' ?>">
            <div class="ui calendar" id="birthdate">
                <?php
                echo $this->Form->input(
                    'birth_date',
                    [
                        'type' => 'text',
                        'div' => false,
                        'between' => '<i class="calendar icon"></i>',
                        'placeholder' => 'Birth Date'
                    ]
                );
                ?>
            </div>
        </div>
        <?php
        echo $this->Form->input(
            'mobile_no',
            [
                'between' => '<i class="phone icon"></i>',
                'placeholder' => 'Mobile Number (Ex. 09XX-XXX-XXXX)',
                'error' => [
                    'attributes' => [
                        'escape' => false,
                        'wrap' => 'div',
                        'class' => 'ui pointing red basic label'
                    ]
                ]
            ]
        );
        ?>
    </div>
    <?php
    echo $this->Form->input(
        'address',
        [
            'between' => '<i class="home icon"></i>',
            'placeholder' => 'Address'
        ]
    );
    ?>
    <div class="field">
        <label>Image Upload:</label>
        <?php
        echo $this->Form->file('profile_picture');
        echo $this->Form->input('profile_picture_dir', ['type' => 'hidden']);
        ?>
        <?php if (isset($errors['profile_picture'])) : ?>
                <div class="ui pointing red basic label">
                    <?php if (count($errors['profile_picture']) > 1) : ?>
                        <ul>
                            <?php
                                    foreach ($errors['profile_picture'] as $value) {
                                        echo "<li>$value</li>";
                                    }
                                    ?>
                        </ul>
                    <?php else : ?>
                        <?php echo $errors['profile_picture'][0]; ?>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
    </div>
    <div class="ui error message"></div>
    <div class="ui divider"></div>
    <div class="ui basic center aligned segment">
        <?php
        echo $this->Form->button('Update <i class="checkmark icon"></i> ', [
            'type' => 'submit',
            'class' => 'ui positive right labeled icon button'
        ]);
        ?>
    </div>
    <?php echo $this->Form->end(); ?>
</div>

<div class="ui raised teal segment">
    <div class="ui center aligned header teal icon">
        <i class="lock icon"></i>
        Change Password
    </div>
    <?php echo $this->Form->create(
        'User',
        [
            'class' => 'ui large form',
            'url' => [
                'controller' => 'profile',
                'action' => 'edit_password'
            ],
            'inputDefaults' => [
                'label' => false,
                'div' => ['class' => 'field'],
                'before' => '<div class="ui left icon input">',
                'after' => '</div>'
            ],
            'type' => 'post',
            'novalidate' => 'novalidate'
        ]
    );
    ?>
    <?php
    echo $this->Form->input(
        'old_password',
        [
            'between' => '<i class="lock icon"></i>',
            'placeholder' => 'Current Password',
            'type' => 'password'
        ]
    );
    echo $this->Form->input(
        'password',
        [
            'between' => '<i class="lock icon"></i>',
            'placeholder' => 'New Password'
        ]
    );
    echo $this->Form->input(
        'password_confirmation',
        [
            'required' => true,
            'between' => '<i class="lock icon"></i>',
            'placeholder' => 'Confirm Password',
            'type' => 'password'
        ]
    );
    ?>
    <div class="ui error message"></div>
    <div class="ui divider"></div>
    <div class="ui basic center aligned segment">
        <?php
        echo $this->Form->button('Change <i class="checkmark icon"></i> ', [
            'type' => 'submit',
            'class' => 'ui positive right labeled icon button'
        ]);
        ?>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
<script>
    $('#birthdate').calendar({
        type: 'date',
        formatter: {
            date: function(date, settings) {
                if (!date) return '';
                var day = date.getDate();
                var month = date.getMonth() + 1;
                var year = date.getFullYear();
                return year + '-' + month + '-' + day;
            }
        }
    });
</script>