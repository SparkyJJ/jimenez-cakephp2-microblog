<?php
echo json_encode([
    'content' => $this->Form->postLink(
        '<i class="thumbs up icon"></i><span class="like-content">' . ucfirst($status) . '</span>',
        ['controller' => 'likes', 'action' => $status, $id],
        [
            'escape' => false,
            'style' => 'color: inherit; text-decoration: none;',
            'class' => 'like-anchor'
        ]
    ),
    'count' => $count
]);
