<?php
if(isset($posts)) {
$array['content'] = $this->element('component.feed', ['posts' => $posts]);
}

if(isset($message)) {
    if($message['type'] === 'dimmer') {
        $array['message'] = $this->element('dimmer.message', ['modal' => $message]);
    } else {
        $array['message'] = $this->element('modal.message', ['modal' => $message]);
    }
    $array['messageType'] = $message['type'];
}

echo json_encode($array);
