<?php
class LikesController extends AppController
{
    public $helpers = ['Form', 'Html', 'Js', 'Flash'];

    public $components = [
        'Security' => [
            'csrfUseOnce' => false
        ]
    ];

    public function beforeFilter()
    {
        parent::beforeFilter();
    }
    
    public function like($id = null)
    {
        $this->request->onlyAllow('ajax');
        $this->layout = 'ajax';
        $this->view = '/Ajax/like';
        $this->loadModel('Post');

        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        $this->Like->create();
        $this->Like->set('user_id', $this->Auth->user('id'));
        $this->Like->set('post_id', $id);
        if ($this->Like->save(
            $this->request->data,
            [
                'fieldList' => [
                    'user_id',
                    'post_id'
                ]
            ]
        )) {
            $this->getLikeCount($id, 'like');
        } else {
            throw new ForbiddenException();
        }
    }

    public function unlike($post = null)
    {
        $this->request->onlyAllow('ajax');
        $this->layout = 'ajax';
        $this->view = '/Ajax/like';
        $this->loadModel('Post');

        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }

        $temp = $this->Like->find('first', [
            'fieldList' => ['id'],
            'conditions' => [
                'post_id' => $post,
                'user_id' => $this->Auth->user('id')
            ],
            'recursive' => -1
        ]);

        $this->Like->Behaviors->disable('SoftDelete');
        if ($this->Like->delete($temp['Like']['id'])) {
            $this->getLikeCount($post, 'unlike');
        } else {
            throw new ForbiddenException();
        }
    }

    private function getLikeCount($id, $status) {
        $this->Post->id = $id;
        $likeCount = $this->Post->field('like_count');
        $this->set('id', $id);
        $this->set('status', ($status === 'like') ? 'unlike' : 'like');
        $this->set('count', $likeCount);
    }
}
