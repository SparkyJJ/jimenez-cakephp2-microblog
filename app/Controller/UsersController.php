<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class UsersController extends AppController
{
    public $helpers = ['Form', 'Html', 'Js', 'Flash'];

    public function beforeFilter()
    {
        parent::beforeFilter();
    }

    public function activate($hashed_email = null, $id = null, $hashed_code = null)
    {
        if (isset($hashed_email) && isset($hashed_code) && isset($id)) {
            $this->User->read(null, $id);
            $this->User->data['User']['activation_code'];
            $userEmail = $this->hashMd5($this->User->data['User']['email']);

            if ($userEmail === $hashed_email && $this->User->data['User']['activation_code'] === $hashed_code) {
                $this->User->saveField('activated', true);
                $this->Flash->modal(
                    __('You may now use your account to log in to the blog.'),
                    [
                        'params' => [
                            'title' => 'Account is now activated',
                            'icon' => 'green check circle icon'
                        ]
                    ]
                );
            } else {
                $this->Flash->modal(
                    __('Please contact the administrator.'),
                    [
                        'params' => [
                            'title' => 'Invalid activation link',
                            'icon' => 'red exclamation triangle icon'
                        ]
                    ]
                );
            }
        }

        $this->redirect('/');
    }

    public function reactivate($hashed_code = null, $id = null)
    {
        if (isset($hashed_code) && isset($id)) {
            $this->User->read(null, $id);
            $this->User->data['User']['activation_code'];

            if ($this->User->data['User']['activation_code'] === $hashed_code) {
                //Generate activation link
                $userId = $this->User->id;
                $hashed_email = $this->hashMd5($this->User->data['User']['email']);
                $activation_link = 'http://' . $_SERVER['SERVER_NAME'] . "/users/activate/$hashed_email-$userId-$hashed_code";

                // Send activation email for registered user
                $Email = new CakeEmail();
                $Email->config('gmail')
                    ->template('activate', 'main')
                    ->emailFormat('html')
                    ->to($this->User->data['User']['email'])
                    ->subject('SparkBlog Account Activation')
                    ->viewVars(['url' => $activation_link])
                    ->send();

                $this->Flash->modal(
                    __('Please check your email for activation link'),
                    [
                        'params' => [
                            'title' => 'Activation email sent',
                            'icon' => 'green check circle icon'
                        ]
                    ]
                );
            } else {
                $this->Flash->modal(
                    __('Please contact the administrator.'),
                    [
                        'params' => [
                            'title' => 'Invalid reactivation link',
                            'icon' => 'red exclamation triangle icon'
                        ]
                    ]
                );
            }
        }

        $this->redirect('/');
    }
}
