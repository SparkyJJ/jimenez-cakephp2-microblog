<?php
App::uses('CakeEmail', 'Network/Email');

class HomeController extends AppController
{
    public $helpers = ['Html', 'Form', 'Flash'];
    public $components = ['Flash', 'Session', 'Cookie'];

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow('index', 'register');
    }

    public function index()
    {
        $this->loadModel('User');
        $this->layout = 'home';
        $this->set('home', 'active');
        $this->set('register', '');

        //Redirect to main page if user is already logged in
        if ($this->Auth->loggedIn()) {
            $this->redirect('/main');
        }

        //Check POST request from Login Form
        if ($this->request->is('post')) {

            $user = $this->User->find(
                'first',
                [
                    'conditions' => ['User.username' => $this->request->data['User']['username']],
                    'fields' => 'activated',
                    'recursive' => -1
                ]
            );

            //Login user
            if ($this->Auth->login()) {
                //Check if user is activated
                if ($user['User']['activated'] === true) {
                    return $this->redirect($this->Auth->redirectUrl());
                } else {
                    $this->modalMessage(
                        'Account is not yet activated',
                        'Please check your email for your activation link. <br> 
                        Have not received an email? <a href="/users/reactivate/' .
                            $this->Auth->user('activation_code') . '-' . $this->Auth->user('id') . '" id="reactivate"> Click here </a>',
                        'orange universal access icon'
                    );
                    return $this->redirect($this->Auth->logout());
                }
            } else {
                $this->modalMessage(
                    'Error',
                    'Username or password is incorrect'
                );
            }
        }
    }

    public function register()
    {
        $this->layout = 'home';
        $this->set('home', '');
        $this->set('register', 'active');
        $this->loadModel('User');
        $userFields = [
            'username',
            'password',
            'password_confirmation',
            'first_name',
            'last_name',
            'gender',
            'address',
            'birth_date',
            'mobile_no',
            'email',
            'profile_picture',
            'profile_picture_dir'
        ];

        //Redirect to main page if user is already logged in
        if ($this->Auth->loggedIn()) {
            $this->redirect('/main');
        }

        //Check POST request from registration form
        if ($this->request->is('post')) {

            $this->User->create();

            if ($this->User->save($this->request->data, ['fieldList' => $userFields])) {

                //Generate activation link
                $userId = $this->User->id;
                $hashed_code = $this->hashMd5($userId);
                $hashed_email = $this->hashMd5($this->request->data['User']['email']);
                $activation_link = 'http://' . $_SERVER['SERVER_NAME'] . "/users/activate/$hashed_email-$userId-$hashed_code";
                $this->User->saveField('activation_code', $hashed_code);

                //Send activation email for registered user
                $Email = new CakeEmail();
                $Email->config('gmail')
                    ->template('activate', 'main')
                    ->emailFormat('html')
                    ->to($this->request->data['User']['email'])
                    ->subject('SparkBlog Account Activation')
                    ->viewVars(['url' => $activation_link])
                    ->send();

                $this->modalMessage(
                    'Registration has been saved',
                    'Please check your email for activation.',
                    'green check circle icon'
                );
                return $this->redirect(['action' => 'index']);
            } else {

                //Display error message
                // $error['list'] = $this->User->validationErrors;
                $error = $this->User->validationErrors;
                // $error['header'] = 'There were some errors with your submission';
                $this->set('errors', $error);
                $this->modalMessage(
                    'Error submitting registration',
                    'Please check your submitted fields'
                );
            }
        }
    }

    public function logout()
    {
        header('pragma: no-cache');
        header('Cache-Control: no-cache, must-revalidate');
        $this->response->disableCache();
        $this->Session->delete('Auth.User');
        $this->Session->delete('User');
        $this->Session->destroy();
        $this->Cookie->destroy();
        $this->modalMessage(
            'Log out successful',
            'Come visit us again next time!',
            'green check circle icon'
        );
        return $this->redirect($this->Auth->logout());
    }
}
