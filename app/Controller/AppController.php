<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		https://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    public $components = [
        // 'DebugKit.Toolbar',
        'Security',
        'Flash',
        'Auth' => [
            'loginAction' => [
                'controller' => 'home',
                'action' => 'index'
            ],
            'loginRedirect' => [
                'controller' => 'main',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'home',
                'action' => 'index'
            ],
            'authenticate' => [
                'Form' => [
                    'passwordHasher' => 'Blowfish'
                ]
            ]
        ]
    ];

    public $helpers = [
        'Html' => [
            'className' => 'Baklava.BaklavaHtmlHelper',
        ]
    ];

    protected $basicUserFields = [
        'id',
        'username',
        'first_name',
        'last_name',
        'follower_count',
        'following_count',
        'profile_picture',
        'profile_picture_dir',
        'created'
    ];

    public function beforeFilter()
    {
        $this->Auth->allow('activate', 'reactivate', 'logout');
        if ($this->Auth->loggedIn()) {
            $this->set('user', $this->getBasicUserData());
        }
    }
    /**
     * Concised md5 hashing
     *
     * @param array $value
     * @return void
     */
    public function hashMd5($value)
    {
        return Security::hash(
            $value,
            'md5',
            'my-salt'
        );
    }

    /**
     * Renews current user data, e.g. in case of an email address change while being logged in.
     *
     * @param array $newUserData
     * @return void
     */
    protected function renewUserSession($newUserData)
    {
        if (!isset($newUserData) || empty($newUserData)) {
            return;
        }

        // We need to fetch the current user data so custom indexes are copied
        $currentUserData = $this->Auth->user();
        if (!isset($currentUserData) || empty($currentUserData)) {
            return;
        }

        // Merge old with new data
        $newUserData = array_merge($currentUserData, $newUserData);

        // Login with new data
        $this->Auth->login($newUserData);
    }

    /**
     * Concised Customized Flash Modal
     *
     * @param string $title
     * @param string $message
     * @param string $icon
     * @return void
     */
    protected function modalMessage($title, $message = null, $icon = null)
    {
        $this->Flash->modal(
            $message,
            [
                'params' => [
                    'title' => __($title),
                    'icon' => __($icon)
                ]
            ]
        );
    }

    /**
     * Get user information row without sensitive data and with extra attributes such as isResult and isFollowing
     *
     * @param string $find
     * @param array $conditions
     * @return array
     */
    protected function getBasicUserData($id = null, $options = null)
    {
        $this->loadModel('User');
        $this->loadModel('Follower');

        if (!isset($id)) {
            $id = $this->Auth->user('id');
        }

        $findOptions = [
            'conditions' => ['User.id' => $id],
            'fields' => [
                'User.id',
                'User.username',
                'User.first_name',
                'User.last_name',
                'User.gender',
                'User.address',
                'User.birth_date',
                'User.mobile_no',
                'User.email',
                'User.profile_picture',
                'User.profile_picture_dir',
                'User.following_count',
                'User.follower_count',
                'User.follow_request_count',
                'User.created',
            ]
        ];


        if (isset($options)) {
            $findOptions = array_merge($findOptions, $options);
        }
        $temp = $this->User->find('first', $findOptions);
        if (!empty($temp)) {
            $temp['User']['isUser'] = ($temp['User']['id'] === $this->Auth->user('id'));

            if (!$temp['User']['isUser']) {
                $temp['User']['isFollowing'] = $this->Follower->find('first', [
                    'recursive' => -1,
                    'conditions' =>
                    [
                        'Follower.user_followee_id' => $id,
                        'Follower.user_follower_id' => $this->Auth->user('id')
                    ]
                ]);
            }
        } else {
            $this->modalMessage(
                'No user could be found',
                'Please contact the administrator.'
            );
            $this->redirect('/main');
        }

        return $temp;
    }

    /**
     * Get logged in user's list of followers
     *
     * @return array
     */
    protected function getFollowerList()
    {
        $this->loadModel('Follower');

        //Get list of followers
        $followers = $this->Follower->find('list', [
            'fields' => ['user_followee_id'],
            'conditions' => [
                'Follower.user_follower_id' => $this->Auth->user('id'),
                'Follower.accepted' => 1
            ]
        ]);

        return $followers;
    }

    /**
     * Get visibility conditions for paginate
     *
     * @param mixed
     * @return array
     */
    protected function getVisibilityConditions($options = null)
    {
        if (!isset($options['followerList'])) {
            $options['followerList'] = $this->getFollowerList();
        }

        if (!isset($options['postModel'])) {
            $options['postModel'] = 'Post';
        }

        $conditions = [
            'OR' => [
                [$options['postModel'] . '.user_id' => $this->Auth->user('id')],
                [
                    'AND' => [
                        [
                            'OR' => [
                                [$options['postModel'] . '.visibility' => 0],
                                [$options['postModel'] . '.visibility' => 1]
                            ]
                        ],
                        [$options['postModel'] . '.user_id' => $options['followerList']]
                    ]
                ]
            ]
        ];

        return $conditions;
    }
}
