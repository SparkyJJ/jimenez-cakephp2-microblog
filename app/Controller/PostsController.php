<?php
class PostsController extends AppController
{
    public $helpers = ['Form', 'Html', 'Js', 'Flash', 'Paginator'];


    public $components = [
        'Paginator',
        'Session',
        'Security' => [
            'csrfUseOnce' => false
        ]
    ];

    public $paginate = [
        'limit' => 5,
        'order' => [
            'Comment.id' => 'desc'
        ]
    ];

    public $paginateFeed = [
        'limit' => 10,
        'order' => [
            'Post.id' => 'desc'
        ]
    ];

    public function beforeFilter()
    {
        parent::beforeFilter();
    }

    public function add()
    {
        $this->request->onlyAllow('ajax');
        $this->autoRender = false;

        $postFields = [
            'user_id',
            'user_share_id',
            'post_id',
            'body',
            'image',
            'image_dir',
            'visibility'
        ];
        $this->Post->create();
        if ($this->request->data['Post']['type'] === 'share') {
            unset($this->Post->validate['body']);
        }
        $this->Post->set('user_id', $this->Auth->user('id'));
        if ($this->Post->save($this->request->data, ['fieldList' => $postFields])) {
            $refer_url = $this->referer('/', true);
            $parse_url_params = Router::parse($refer_url);
            if ($parse_url_params['controller'] === 'main') {
                $message = [
                    'title' => 'Post submit successful',
                    'message' =>  '',
                    'icon' => 'green check circle icon',
                    'status' => 'success',
                    'type' => 'modal'
                ];
            } else {
                $this->modalMessage(
                    'Post submit successful',
                    '',
                    'green check circle icon'
                );
            }
        } else {
            $message = [
                'title' => 'Invalid post',
                'message' =>  $this->Post->validationErrors,
                'status' => 'error',
                'type' => 'dimmer'
            ];
        }
        $this->Session->write('messageData', $message);
        $this->redirect(['controller' => 'feed', 'action' => 'view']);
    }

    public function view($id = null)
    {
        $this->layout = 'main';
        $this->loadModel('Follower');

        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        //Get post details
        $post = $this->Post->find('first', [
            'conditions' => ['Post.id' => $id],
            'contain' => [
                'Share_Post' => ['Share_Post', 'Share_User'],
                'Share_User',
                'User' => [
                    'Followee' => [
                        'conditions' => ['user_follower_id' => $this->Auth->user('id')]
                    ]
                ],
                'Like' => [
                    'conditions' => ['Like.user_id' => $this->Auth->user('id')]
                ]
            ]
        ]);

        //Throw Exception when trying to view a not owned "Only Me" visibility post
        if ($post['Post']['visibility'] === '2' && $post['User']['id'] != $this->Auth->user('id')) {
            throw new ForbiddenException(__('Unable to view post'));
        }

        //Throw Exception when trying to view a user's post which is not followed 
        if ($post['Post']['visibility'] === '1' && empty($post['User']['Followee']) && $post['User']['id'] != $this->Auth->user('id')) {
            throw new ForbiddenException(__('Unable to view post'));
        }

        $this->paginate['contain'] = ['User', 'Reply'];
        $this->paginate['conditions'] = ['Comment.post_id' => $id];
        $this->Paginator->settings = $this->paginate;

        $post['Comment'] = $this->Paginator->paginate('Comment');

        if (empty($post['Post'])) {
            throw new NotFoundException(__('Invalid post'));
        }

        $this->set('post', $post);
    }

    public function edit($id = null)
    {
        $this->layout = 'main';

        //Set post form settings
        $postForm = ['title' => 'Edit Post', 'action' => 'edit', 'button' => 'Save', 'type' => 'edit'];

        $postFields = [
            'user_id',
            'body',
            'image',
            'image_dir',
            'visibility'
        ];

        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        $post = $this->Post->find('first', [
            'conditions' => ['Post.id' => $id],
            'contain' => [
                'Share_Post' => ['Share_Post', 'Share_User'],
                'Share_User',
                'User',
                'Like' => [
                    'conditions' => ['Like.user_id' => $this->Auth->user('id')]
                ]
            ]
        ]);

        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }

        //For showing shared post in post form
        if (isset($post['Post']['post_id'])) {
            $share['Post'] = $post['Share_Post'];
            $share['User'] = $post['Share_User'];
            $share['Share_Post'] = $post['Share_Post']['Share_Post'];
            $share['Share_User'] = $post['Share_Post']['Share_User'];
            $share['isShare'] = true;
            $postForm['share'] = $share;
        }
        $this->set('postForm', $postForm);

        //Check if own the post being edited
        if ($post['User']['id'] === $this->Auth->user('id')) {
            //Check if POST request
            if ($this->request->is(['post', 'put'])) {
                //Save updated fields to database
                $this->Post->id = $id;
                if ($this->Post->save($this->request->data, ['fieldList' => $postFields])) {
                    $this->modalMessage(
                        'Update Successful',
                        'Post has been updated.',
                        'green check circle icon'
                    );

                    return $this->redirect(['action' => 'view', $id]);
                } else {
                    $this->request->data = $post;
                    $this->set('errors', $this->Post->validationErrors);
                    $this->modalMessage(
                        'Error',
                        'Please check your submitted fields'
                    );
                }
            } else {
                $this->request->data = $post;
            }
        } else {
            $this->modalMessage(
                'Unable to access post',
                'You are not allowed to edit this post'
            );
            return $this->redirect('/main');
        }
    }

    public function delete($id = null)
    {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }

        if ($this->Post->delete($id)) {
            $this->modalMessage(
                'Post is now deleted',
                'Your post is deleted successfully',
                'green check circle icon'
            );
        } else {
            $this->modalMessage(
                'Unable to delete post',
                'Please contact the administrator.'
            );
        }

        return $this->redirect('/main');
    }

    //Ajax Action for share post form to be enclosed by a modal
    public function view_share($post_id = null, $user_id = null)
    {
        $this->request->onlyAllow('ajax');
        $this->layout = 'ajax';
        $this->view = '/Ajax/postfield';
        $this->loadModel('Post');

        if (!$post_id) {
            throw new NotFoundException(__('Invalid post'));
        }

        if (!$user_id) {
            throw new NotFoundException(__('Invalid user'));
        }

        $post = $this->Post->find('first', [
            'conditions' => ['Post.id' => $post_id],
            'contain' => [
                'Share_Post' => ['Share_Post', 'Share_User'],
                'Share_User',
                'User',
                'Like' => [
                    'conditions' => ['Like.user_id' => $this->Auth->user('id')]
                ]
            ]
        ]);

        $post['isShare'] = true;
        $postForm = [
            'title' => 'Share Post',
            'action' => 'add',
            'button' => 'Share',
            'share' => $post,
            'type' => 'share'
        ];
        $this->set('postForm', $postForm);
        $this->request->data['Post']['post_id'] = $post_id;
        $this->request->data['Post']['user_share_id'] = $user_id;
    }
}
