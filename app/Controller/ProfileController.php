<?php
App::uses('AppController', 'Controller');

class ProfileController extends AppController
{
    public $helpers = ['Form', 'Html', 'Js', 'Flash'];

    public $components = ['Paginator', 'Session'];

    public $paginate = [
        'limit' => 10,
        'order' => [
            'Post.id' => 'desc'
        ]
    ];

    public function beforeFilter()
    {
        parent::beforeFilter();
    }

    public function view($id = null)
    {
        $this->layout = 'main';
        $this->loadModel('User');
        $this->loadModel('Post');
        $this->loadModel('Follower');

        if (!isset($id)) {
            $id = $this->Auth->user('id');
        }

        $userData = $this->getBasicUserData($id);
        $this->set('viewUserData', $userData['User']);

        $followers = $this->getFollowerList();

        $sharePostConditions = $this->getVisibilityConditions([
            'followerList' => $followers,
            'postModel' => 'Share_Post'
        ]);

        $this->paginate['conditions'] = [
            'AND' => [
                ['Post.user_id' => $id],
                [
                    'OR' => [
                        ['Post.user_id' => $this->Auth->user('id')],
                        ['Post.visibility' => 0],
                        [
                            'AND' => [
                                ['Post.visibility' => 1],
                                ['Post.user_id' => $followers]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $this->paginate['contain'] = [
            'Share_Post' => [
                'Share_Post' => [
                    'conditions' => $sharePostConditions
                ],
                'Share_User',
                'conditions' => $sharePostConditions
            ],
            'Share_User',
            'User',
            'Like' => [
                'conditions' => ['Like.user_id' => $this->Auth->user('id')]
            ]
        ];

        $this->Paginator->settings = $this->paginate;
        $this->Session->write('feedPaginateSettings', $this->paginate);

        $temp = $this->Paginator->paginate('Post');

        $this->set('posts', $temp);
    }

    public function edit()
    {
        $this->layout = 'main';
        $this->loadModel('User');

        //Check if POST request
        if ($this->request->is(['post', 'put'])) {
            //Save updated fields to database
            $id = $this->Auth->user('id');
            $this->User->id = $id;
            if ($this->User->save(
                $this->request->data,
                [
                    'fieldList' => [
                        'first_name',
                        'last_name',
                        'gender',
                        'address',
                        'birth_date',
                        'mobile_no',
                        'profile_picture',
                        'profile_picture_dir'
                    ]
                ]
            )) {
                $this->modalMessage(
                    'Update Successful',
                    'Profile has been updated.',
                    'green check circle icon'
                );

                $temp = $this->User->read(null, $id);
                $this->renewUserSession($temp['User']);
                return $this->redirect(['action' => 'edit']);
            } else {
                $error = $this->User->validationErrors;
                $this->set('errors', $error);
                $this->modalMessage(
                    'Error',
                    'Please check your submitted fields'
                );
                $this->request->data['User'] = $this->Auth->user();
                unset($this->request->data['User']['password']);
            }
        } else {
            $this->request->data['User'] = $this->Auth->user();
            unset($this->request->data['User']['password']);
        }
    }

    public function edit_password($id = null)
    {
        $this->loadModel('User');
        //Check if passed ID is same with logged in ID
        if ($id !== $this->Auth->user('id')) {
            $this->request->data['User'] = $this->Auth->user();
            unset($this->request->data['User']['password']);
        } elseif ($id === $this->Auth->user('id')) {
            //Check if POST request
            if ($this->request->is(['post', 'put'])) {
                $this->User->id = $id;
                if ($this->User->save(
                    $this->request->data,
                    [
                        'fieldList' => [
                            'password',
                            'old_password',
                            'password_confirmation'
                        ]
                    ]
                )) {
                    $this->modalMessage(
                        'Password changed',
                        'Update successful',
                        'green check circle icon'
                    );
                    $temp = $this->User->read(null, $id);
                    $this->renewUserSession($temp['User']);
                } else {
                    $errors = $this->User->validationErrors;
                    $this->Flash->modal($errors, [
                        'params' => [
                            'title' => 'Error changing password.'
                        ]
                    ]);
                }
            } else {
                $this->request->data['User'] = $this->Auth->user();
            }
        }
        return $this->redirect(['action' => 'edit']);
    }
}
