<?php
App::uses('AppController', 'Controller');

class MainController extends AppController
{
    public $helpers = ['Form', 'Html', 'Js', 'Flash', 'Paginator'];

    public $components = ['Paginator', 'Session'];

    public $paginate = [
        'limit' => 8,
        'order' => [
            'Post.id' => 'desc'
        ]
    ];

    public function beforeFilter()
    {
        parent::beforeFilter();
    }

    public function index()
    {
        $this->layout = 'main';
        $this->loadModel('Post');
        $this->loadModel('Follower');

        //Set post form settings
        $postForm = ['title' => 'Create Post', 'action' => 'add', 'button' => 'Post', 'type' => 'add'];
        $this->set('postForm', $postForm);

        //Get list of followers
        $followers = $this->getFollowerList();

        $sharePostConditions = $this->getVisibilityConditions([
            'followerList' => $followers,
            'postModel' => 'Share_Post'
        ]);

        //Other paginate settings
        $this->paginate['conditions'] = $this->getVisibilityConditions(['followerList' => $followers]);

        $this->paginate['contain'] = [
            'Share_Post' => [
                'Share_Post' => [
                    'conditions' => $sharePostConditions
                ],
                'Share_User',
                'conditions' => $sharePostConditions
            ],
            'Share_User',
            'User',
            'Like' => [
                'conditions' => ['Like.user_id' => $this->Auth->user('id')]
            ]
        ];
        $this->Paginator->settings = $this->paginate;
        $this->Session->write('feedPaginateSettings', $this->paginate);

        $temp = $this->Paginator->paginate('Post');
        $followers[] = $this->Auth->user('id');
        $this->set('posts', $temp);
    }
}
