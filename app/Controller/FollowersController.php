<?php
App::uses('AppController', 'Controller');

class FollowersController extends AppController
{
    public $helpers = ['Form', 'Html', 'Js', 'Flash', 'Paginator'];

    public $components = ['Paginator'];

    public $paginate = [
        'limit' => 25,
        'order' => [
            'Follower.id' => 'asc'
        ]
    ];

    public function beforeFilter()
    {
        parent::beforeFilter();
    }

    public function following($id = null)
    {
        $this->layout = 'main';
        $this->view = '/Main/follow';
        $this->set('header', 'Following');
        $userType = 'follower';
        $this->set('userType', $userType);
        $viewData = $this->getBasicUserData($id);
        $this->set('viewUserData', $viewData['User']);

        $data = $this->getPaginatedFollow($id, $userType);
        $this->set('follows', $data);
    }

    public function followers($id = null)
    {
        $this->layout = 'main';
        $this->view = '/Main/follow';
        $this->set('header', 'Followers');
        $userType = 'followee';
        $this->set('userType', $userType);
        $viewData = $this->getBasicUserData($id);
        $this->set('viewUserData', $viewData['User']);

        $data = $this->getPaginatedFollow($id, $userType);
        $this->set('follows', $data);
    }

    public function pending()
    {
        $this->layout = 'main';
        $this->view = '/Main/follow';
        $this->set('header', 'Pending');
        $userType = 'followee';
        $this->set('userType', $userType);
        $viewData = $this->getBasicUserData($this->Auth->user('id'));
        $this->set('viewUserData', $viewData['User']);

        $data = $this->getPaginatedFollow($this->Auth->user('id'), $userType, 0);
        $this->set('follows', $data);
    }

    public function accept($id = null, $username = null)
    {
        if (!$id) {
            throw new NotFoundException();
        }

        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }

        $temp = $this->Follower->find('first', [
            'fieldList' => ['id'],
            'conditions' => [
                'Follower.user_followee_id' => $this->Auth->user('id'),
                'Follower.user_follower_id' => $id
            ],
            'recursive' => -1
        ]);

        if(isset($temp['Follower']['id'])) {
            $this->Follower->read(null, $temp['Follower']['id']);
            $this->Follower->saveField('accepted', true);
            $this->modalMessage(
                "You just accepted $username's follow",
                "$username will be able to see your public and followed post.",
                'green check circle icon'
            );
        } else {
            $this->modalMessage(
                "Invalid user.",
                'Please contact the administrator.'
            );
        }

        return $this->redirect($this->referer());
    }

    public function decline($id = null, $username = null)
    {
        if (!$id) {
            throw new NotFoundException();
        }

        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }

        $temp = $this->Follower->find('first', [
            'fieldList' => ['id'],
            'conditions' => [
                'Follower.user_followee_id' => $this->Auth->user('id'),
                'Follower.user_follower_id' => $id
            ],
            'recursive' => -1
        ]);

        $this->Follower->Behaviors->disable('SoftDelete');
        if ($this->Follower->delete($temp['Follower']['id'])) {
            $this->modalMessage(
                "You just declined $username's follow",
                "Your followed post won\'t be able to be seen by $username",
                'green check circle icon'
            );
        } else {
            $this->modalMessage(
                "Unable to decline $username",
                'Please contact the administrator.'
            );
        }

        return $this->redirect($this->referer());
    }

    public function follow($id = null, $username = null)
    {
        if (!$id) {
            throw new NotFoundException();
        }

        if (!$username) {
            throw new NotFoundException();
        }

        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }

        if ($this->request->is('post')) {
            $this->Follower->create();
            $this->Follower->set('user_follower_id', $this->Auth->user('id'));
            $this->Follower->set('user_followee_id', $id);
            if ($this->Follower->save()) {
                $this->modalMessage(
                    "You have sent a follow request to $username",
                    'Please wait for the user to accept/decline your request.',
                    'green check circle icon'
                );
            }
            return $this->redirect($this->referer());
        }

        return $this->redirect('/main');
    }

    public function unfollow($id = null, $username = null, $type = 0)
    {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }

        if (!$id) {
            throw new NotFoundException();
        }

        if (!$username) {
            throw new NotFoundException();
        }

        $temp = $this->Follower->find('first', [
            'fieldList' => ['id'],
            'conditions' => [
                'Follower.user_followee_id' => $id,
                'Follower.user_follower_id' => $this->Auth->user('id')
            ],
            'recursive' => -1
        ]);

        $this->Follower->Behaviors->disable('SoftDelete');
        if ($this->Follower->delete($temp['Follower']['id'])) {
            if($type === '0') {
                $this->modalMessage(
                    "You just unfollowed $username",
                    'You won\'t be able to see his/her private post anymore.',
                    'green check circle icon'
                );
            } else {
                $this->modalMessage(
                    "You just unfollowed $username",
                    'Follow request removed.',
                    'green check circle icon'
                );
            }
        } else {
            $this->modalMessage(
                "Unable to unfollow $username",
                'Please contact the administrator.'
            );
        }

        return $this->redirect($this->referer());
    }

    private function getUserFollow($id = null, $type)
    {
        if (!isset($id)) {
            $id = $this->Auth->user('id');
        }

        $this->loadModel('User');

        $options = [
            'contain' => [
                ucfirst($type) . '.user_' . $type . '_id' =>
                [
                    'user_' . $type =>
                    [
                        'fields' => $this->basicUserFields
                    ]
                ]
            ]
        ];

        $temp = $this->getBasicUserData($id, $options);
        return $temp;
    }

    private function getPaginatedFollow($id = null, $type, $accepted = 1)
    {
        $this->paginate['conditions'] = [
            'user_' . $type . '_id' => $id,
            'accepted' => $accepted
        ];
        $this->paginate['fields'] = ['id'];
        $this->paginate['contain'] = [
            'user_' . $type =>
            [
                'fields' => $this->basicUserFields
            ]
        ];
        $this->Paginator->settings = $this->paginate;

        $temp = $this->Paginator->paginate('Follower');

        return $temp;
    }
}
