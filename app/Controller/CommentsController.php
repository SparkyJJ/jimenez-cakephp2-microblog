<?php
class CommentsController extends AppController
{
    public $helpers = ['Form', 'Html', 'Js', 'Flash', 'Paginator'];

    public $paginateComments = [
        'limit' => 3,
        'order' => [
            'Comment.id' => 'desc'
        ]
    ];

    public $components = array(
        'Security' => [
            'csrfUseOnce' => false
        ],
        'Paginator'
    );

    public function beforeFilter()
    {
        parent::beforeFilter();
    }

    public function add($post_id = null, $comment_id = null)
    {
        $this->request->onlyAllow('ajax');
        $this->layout = 'ajax';
        if (isset($comment_id)) {
            $this->view = '/Ajax/comment';
        } else {
            $this->view = '/Ajax/comment.feed';
        }
        $this->loadModel('Post');

        if (!$post_id) {
            throw new NotFoundException(__('Invalid post'));
        }

        $this->Comment->create();
        $this->Comment->set('user_id', $this->Auth->user('id'));
        $this->Comment->set('post_id', $post_id);
        $this->Comment->set('comment_id', $comment_id);
        if ($this->Comment->save(
            $this->request->data,
            [
                'fieldList' => [
                    'user_id',
                    'post_id',
                    'comment_id',
                    'comment',
                ]
            ]
        )) {
            if (!isset($comment_id)) {

                $this->paginateComments['conditions'] = [
                    'Comment.post_id' => $post_id,
                    'comment_id' => null
                ];

                $this->paginateComments['contain'] = [
                    'User',
                    'Reply' => [
                        'User',
                        'conditions' => ['deleted' => 0]
                    ],
                ];
                $this->Paginator->settings = $this->paginateComments;

                $post['Post']['id'] = $post_id;
                $post['Comment'] = $this->Paginator->paginate('Comment');
                $this->set('post', $post);
            } else {
                $comment = $this->Comment->find('first', [
                    'conditions' => ['Comment.id' => $this->Comment->getLastInsertId()],
                    'contain' => ['User']
                ]);
                $this->set('content', $comment);
            }

            $count = $this->Post->field('comment_count', ['id' => $post_id]);
            $this->set('count', $count);
        } else {
            throw new ForbiddenException();
        }
    }

    public function edit($id = null, $post_id = null)
    {
        $this->request->onlyAllow('ajax');
        $this->layout = 'ajax';
        $this->view = '/Ajax/comment';
        $this->loadModel('Post');

        if (!$id) {
            throw new NotFoundException(__('Invalid comment'));
        }

        if (!$post_id) {
            throw new NotFoundException(__('Invalid post'));
        }

        //Check if POST request
        if ($this->request->is(['post', 'put'])) {
            //Save updated fields to database
            if ($this->Comment->save(
                $this->request->data,
                [
                    'fieldList' => [
                        'user_id',
                        'post_id',
                        'comment_id',
                        'comment',
                    ]
                ]
            )) {
                $comment = $this->Comment->find('first', [
                    'conditions' => ['Comment.id' => $this->Comment->id],
                    'contain' => [
                        'User'
                    ]
                ]);
                unset($comment['Reply']);
                $this->set('content', $comment);
                $count = $this->Post->field('comment_count', ['id' => $post_id]);
                $this->set('count', $count);
            } else {
                throw new ForbiddenException();
            }
        } else {
            throw new ForbiddenException();
        }
    }

    public function delete($id = null, $post_id = null)
    {
        $this->loadModel('Post');
        $this->request->onlyAllow('ajax');
        $this->layout = 'ajax';
        $this->view = '/Ajax/comment.feed';

        if (!$id) {
            throw new NotFoundException(__('Invalid comment'));
        }

        $count = $this->Comment->find('count', [
            'conditions' => [
                'Comment.id' => $id,
                'Comment.user_id' => $this->Auth->user('id')
            ]
        ]);

        if ($count == 0) {
            throw new ForbiddenException();
        }
        $comment = $this->Comment->find('first', ['conditions' => ['Comment.id' => $id]]);
        if ($this->Comment->delete($id)) {
            if (isset($comment['Comment']['comment_id'])) {
                $this->Post->updateAll(
                    ['Post.comment_count' => 'Post.comment_count-1'],
                    ['Post.id' => $post_id]
                );
                $this->Comment->updateAll(
                    ['Comment.comment_count' => 'Comment.comment_count-1'],
                    ['Comment.id' => $comment['Comment']['comment_id']]
                );
            } else {
                $date = date('Y-m-d H:i:s');

                $this->Post->updateAll(
                    ['Post.comment_count' => 'Post.comment_count-' . ($comment['Comment']['comment_count'] + 1)],
                    ['Post.id' => $post_id]
                );

                $this->Comment->updateAll(
                    [
                        'Comment.deleted' => 1,
                        'Comment.deleted_date' => "\"" . $date . "\""
                    ],
                    ['Comment.comment_id' => $id]
                );
            }
            $this->paginateComments['conditions'] = [
                'Comment.post_id' => $post_id,
                'comment_id' => null
            ];

            $this->paginateComments['contain'] = [
                'User',
                'Reply' => [
                    'User',
                    'conditions' => ['deleted' => 0]
                ],
            ];
            $this->Paginator->settings = $this->paginateComments;

            $post['Post']['id'] = $post_id;
            $post['Comment'] = $this->Paginator->paginate('Comment');
            $this->set('post', $post);

            $count = $this->Post->field('comment_count', ['id' => $post_id]);
            $this->set('count', $count);
        } else {
            throw new ForbiddenException();
        }
    }

    public function view_reply_field($id = null, $post_id = null)
    {
        $this->request->onlyAllow('ajax');
        $this->layout = 'ajax';
        $this->view = '/Elements/component.comment.reply.field';

        if (!$id) {
            throw new NotFoundException(__('Invalid comment'));
        }

        if (!$post_id) {
            throw new NotFoundException(__('Invalid comment'));
        }

        $content['comment_id'] = $id;
        $content['post_id'] = $post_id;
        $content['placeholder'] = 'Reply...';
        $content['type'] = 'add';
        $content['class'] = 'reply';
        $comment['Comment'] = [
            'comment_id' => $id,
            'post_id' => $post_id,
        ];
        $this->request->data = $comment;
        $this->set('content', $content);
    }

    public function view_edit_field($id = null, $post_id = null)
    {
        $this->request->onlyAllow('ajax');
        $this->layout = 'ajax';
        $this->view = '/Elements/component.comment.reply.field';

        if (!$id) {
            throw new NotFoundException(__('Invalid comment'));
        }

        if (!$post_id) {
            throw new NotFoundException(__('Invalid comment'));
        }

        $content['comment_id'] = $id;
        $content['post_id'] = $post_id;
        $content['placeholder'] = '';
        $content['type'] = 'edit';
        $content['class'] = 'edit';

        $comment = $this->Comment->find('first', [
            'conditions' => [
                'Comment.id' => $id,
                'Comment.post_id' => $post_id,
            ]
        ]);
        $this->request->data = $comment;
        $this->set('content', $content);
    }

    public function paginated_comments($id = null)
    {
        $this->loadModel('Comment');
        $this->layout = 'ajax';
        $this->view = '/Elements/component.comment.feed';

        if (!$id) {
            throw new NotFoundException(__('Invalid comment'));
        }

        if (!(!empty($this->request->params['requested']) || $this->request->is('ajax'))) {
            throw new ForbiddenException();
        }

        $this->paginateComments['conditions'] = [
            'Comment.post_id' => $id,
            'comment_id' => null
        ];

        $this->paginateComments['contain'] = [
            'User',
            'Reply' => [
                'User',
                'conditions' => ['deleted' => 0]
            ],
        ];
        $this->Paginator->settings = $this->paginateComments;

        $post['Post']['id'] = $id;
        $post['Comment'] = $this->Paginator->paginate('Comment');
        $this->set('post', $post);
    }
}
