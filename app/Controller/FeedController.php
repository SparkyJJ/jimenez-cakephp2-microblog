<?php
App::uses('AppController', 'Controller');

class FeedController extends AppController
{
    public $helpers = ['Form', 'Html', 'Js', 'Flash', 'Paginator'];

    public $components = ['Paginator', 'Session'];

    public $paginate = [
        'limit' => 8,
        'order' => [
            'Post.id' => 'desc'
        ]
    ];

    public function beforeFilter()
    {
        parent::beforeFilter();
    }

    public function view()
    {
        $this->request->onlyAllow('ajax');
        $this->layout = 'ajax';
        $this->view = '/Ajax/feed';
        $this->loadModel('Post');

        if($this->Session->read('messageData.status') !== 'error') {
            $this->Paginator->settings = $this->Session->read('feedPaginateSettings');
            $temp = $this->Paginator->paginate('Post');
            $this->set('posts', $temp);
        }

        $this->set('message', $this->Session->read('messageData'));
        $this->Session->delete('messageData');
    }
}
