<?php
App::uses('AppController', 'Controller');

class SearchController extends AppController
{

    public $helpers = ['Form', 'Html', 'Js', 'Flash', 'Paginator'];

    public $components = [
        'Security' => [
            'csrfUseOnce' => false
        ],
        'Paginator',
        'Session'
    ];

    public $paginateUser = [
        'recursive' => -1,
        'limit' => 10,
        'order' => [
            'User.id' => 'desc'
        ]
    ];

    public $paginatePost = [
        'limit' => 5,
        'order' => [
            'Post.id' => 'desc'
        ]
    ];

    public function beforeFilter()
    {
        parent::beforeFilter();
    }

    public function index()
    {
        $this->layout = 'main';
        $this->loadModel('User');
        $this->loadModel('Post');
        if (isset($this->request->data['Search']['search_item'])) {
            $searchItem = trim($this->request->data['Search']['search_item']);
        } else {
            $searchItem = '';
        }
        $this->set('headerItem', $searchItem);
        if ($this->request->is('post')) {

            //Get list of followers
            $followers = $this->getFollowerList();

            $users = $this->User->find(
                'all',
                [
                    'recursive' => -1,
                    'limit' => 3,
                    'conditions' => [
                        'activated' => 1,
                        'OR' => [
                            'username LIKE' => '%' . $searchItem . '%',
                            'first_name LIKE' => '%' . $searchItem . '%',
                            'last_name LIKE' => '%' . $searchItem . '%',
                            'CONCAT(first_name, " ", last_name) LIKE' => '%' . $searchItem . '%'
                        ]
                    ],
                    'contain' => [
                        'Followee' => [
                            'conditions' => [
                                'user_follower_id' => $this->Auth->user('id')
                            ]
                        ]
                    ]
                ]
            );
            $this->set('users', $users);

            $sharePostConditions = $this->getVisibilityConditions([
                'followerList' => $followers,
                'postModel' => 'Share_Post'
            ]);

            $posts = $this->Post->find(
                'all',
                [
                    'limit' => 3,
                    'conditions' => [
                        'AND' => [
                            ['Post.body LIKE' => '%' . $searchItem . '%'],
                            [
                                'OR' => [
                                    ['Post.user_id' => $this->Auth->user('id')],
                                    ['Post.visibility' => 0],
                                    [
                                        'AND' => [
                                            ['Post.visibility' => 1],
                                            ['Post.user_id' => $followers]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    'contain' => [
                        'Share_Post' => [
                            'Share_Post' => [
                                'conditions' => $sharePostConditions
                            ],
                            'Share_User',
                            'conditions' => $sharePostConditions
                        ],
                        'Share_User',
                        'User',
                        'Like' => [
                            'conditions' => ['Like.user_id' => $this->Auth->user('id')]
                        ]
                    ]
                ]
            );
            $this->set('posts', $posts);
        }
    }

    public function user()
    {
        $this->layout = 'main';
        $this->loadModel('User');

        if (isset($this->request->data['Search']['search_item'])) {
            $searchItem = trim($this->request->data['Search']['search_item']);
        } else {
            $searchItem = '';
        }
        $this->set('headerItem', $searchItem);

        $this->paginateUser['conditions'] = [
            'activated' => 1,
            'OR' => [
                'username LIKE' => '%' . $searchItem . '%',
                'first_name LIKE' => '%' . $searchItem . '%',
                'last_name LIKE' => '%' . $searchItem . '%',
                'CONCAT(first_name, " ", last_name) LIKE' => '%' . $searchItem . '%'
            ]
        ];

        $this->Paginator->settings = $this->paginateUser;

        $temp = $this->Paginator->paginate('User');

        $this->set('users', $temp);
    }

    public function post()
    {
        $this->layout = 'main';
        $this->loadModel('Post');

        if (isset($this->request->data['Search']['search_item'])) {
            $searchItem = trim($this->request->data['Search']['search_item']);
        } else {
            $searchItem = '';
        }
        $this->set('headerItem', $searchItem);

        //Get list of followers
        $followers = $this->getFollowerList();

        $sharePostConditions = $this->getVisibilityConditions([
            'followerList' => $followers,
            'postModel' => 'Share_Post'
        ]);

        $this->paginatePost['conditions'] = [
            'AND' => [
                ['Post.body LIKE' => '%' . $searchItem . '%'],
                [
                    'OR' => [
                        ['Post.user_id' => $this->Auth->user('id')],
                        ['Post.visibility' => 0],
                        [
                            'AND' => [
                                ['Post.visibility' => 1],
                                ['Post.user_id' => $followers]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        
        $this->paginatePost['contain'] = [
            'Share_Post' => [
                'Share_Post' => [
                    'conditions' => $sharePostConditions
                ],
                'Share_User',
                'conditions' => $sharePostConditions
            ],
            'Share_User',
            'User',
            'Like' => [
                'conditions' => ['Like.user_id' => $this->Auth->user('id')]
            ]
        ];

        $this->Paginator->settings = $this->paginatePost;
        $this->Session->write('feedPaginateSettings', $this->paginatePost);

        $temp = $this->Paginator->paginate('Post');

        $this->set('posts', $temp);
    }
}
